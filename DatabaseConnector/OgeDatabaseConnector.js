const Promise    = require('promise')
const mysql      = require('mysql')

module.exports.executeQuery = executeQuery


const ReviewDbInteractor    = require('./OgeReviewDatabase')
module.exports.reviewclazz = ReviewDbInteractor.reviewclazz

module.exports.reviewMaster = ReviewDbInteractor.reviewMaster
module.exports.fetchMasterReview = ReviewDbInteractor.fetchMasterReview

module.exports.reviewGenius = ReviewDbInteractor.reviewGenius
module.exports.fetchGeniusReview = ReviewDbInteractor.fetchGeniusReview


let databaseSettings = {
    "develop": {
      "user": "root",
      "password": "ogeniigrowsfast",
      "database": "ogenii_dev",
      "host": "127.0.0.1",
      "dialect": "mysql", 
      "port": "3306", 
      "multipleStatements": true,
      "dateStrings": true,
    },
    "remote": {
      "user": "ogenii_admin",
      "password": "ogeniigrowsfast",
      "database": "ogenii_production",
      "host": "127.0.0.1",
      "dialect": "mysql",
      "multipleStatements": true,
      "dateStrings": true,
    }
  }

let currentEnvironment = 'develop'
let currentDatabase
switch (currentEnvironment) {
    case 'remote':
        currentDatabase = databaseSettings.remote
        break
    case 'develop':
        currentDatabase = databaseSettings.develop
        break
    default: break
}

module.exports.current = currentDatabase


function createConnection() {
    return mysql.createConnection(currentDatabase)
}

async function executeQuery(queryString) {
  
    return new Promise(function(resolve, reject) {

        var connection = createConnection()  
        queryString = queryString.replace(/"undefined"/g, connection.escape(null))
        queryString = queryString.replace(/undefined/g, connection.escape(null))        
        connection.query(queryString, function(err, results, fields) {
            
            connection.end()
            if (err != null) {
                reject(err)
                log('query: \n' + queryString + ' \nfail with err', err)
            }
            else {
                log('query: \n' + queryString + ' \nsuccess')
                resolve(results, fields)
            }
        })
    })
}
  

