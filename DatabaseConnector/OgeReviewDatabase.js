let Tables                = require('../DatabaseConnector/OgeDatabaseTables')
module.exports.fetchclazzReview = fetchclazzReview 
module.exports.reviewclazz = reviewclazz

module.exports.reviewMaster = reviewMaster
module.exports.fetchMasterReview = fetchMasterReview

module.exports.reviewGenius = reviewGenius
module.exports.fetchGeniusReview = fetchGeniusReview 

const itemsOnPage = 5 


//___________________________________________________ clazz ________________________________________

function fetchclazzReview(clazzId, page) {

    var offset = page <= 1 ? 0 : page - 1

    let fetch = 'SELECT * FROM ' + Tables.dbclazzReview + ' review, ' + Tables.dbclazzReviewHistory + ' reviewHistory'
    fetch    += ' WHERE review.clazz_id=reviewHistory.clazz_id AND review.clazz_id=' + clazzId 
    fetch    += ' LIMIT ' + itemsOnPage + ' OFFSET ' + offset * itemsOnPage

    let foundReviews
    return new Promise(function(resolve, reject) {

        database.executeQuery(fetch)
        .then(
            (reviews) => {
                foundReviews = reviews
                return countNumberOfclazzReview(clazzId)
            })
        .then(
            numberOfReviews => {
                const pagination = {
                    current_page: page <= 0 ? 1 : page, 
                    total_pages: parseInt(numberOfReviews / 5 + 1)
                }

                const returnData = {
                    data: foundReviews,
                    pagination: pagination
                }

                resolve(returnData)
        })
        .catch(
            err => {
                reject(err)
        })
    })
}

function countNumberOfclazzReview(clazzId) {
    let countStatement = 'SELECT COUNT(giver_id) as count FROM ' + Tables.dbclazzReviewHistory + ' WHERE clazz_id=' + clazzId
    return new Promise(function(resolve, reject) {

        database.executeQuery(countStatement)
        .then(
            numberOfReview => {
                resolve(numberOfReview[0].count)
        })
    })
    
}

function reviewclazz(genius, clazzId, usefulValue, feeValue, comment) {

    // check if genius did review => reject 
    // get review from dbclazzReview
    // calculate review value 
    // insert to dbclazzReview
    // insert to dbclazzReviewHistory

    let finalReview 

    log('review clazz from genius', genius)
    return new Promise(function(resolve, reject) {

        didGeniusReviewclazz(genius.genius_id, clazzId)
        .then(
            didReview => {
                if(didReview == true) {
                    return fetchReviewOfclazzId(clazzId)
                }
                reject(403)
            })
        .then(
            review => {
                return calculateclazzReviewFigure(review, usefulValue, feeValue)
            })
        .then(
            newReview => {
                finalReview = newReview

                const geniusRateValue = (newReview.fee_value + newReview.useful_value) / 2
                return insertIntoReviewHistoryOfclazzFromGeniusComment(clazzId, genius, geniusRateValue, comment)
            })
        .then(
            () => {
                log('clazz review exist', finalReview.exist)
                return finalReview.exist == true ? 
                    updateReviewOfclazz(clazzId, finalReview.rate_value, finalReview.useful_value, finalReview.fee_value, finalReview.rate_count) : 
                    insertReviewOfclazz(clazzId, finalReview.rate_value, finalReview.useful_value, finalReview.fee_value, finalReview.rate_count)
            })
        .then(
            () => {
                resolve()
            })
        .catch(
            err => {
                log('give review had err ', err)
            })
    })
}

function calculateclazzReviewFigure(currentReview, usefulValue, feeValue) {

    return new Promise(function(resolve, reject) {
        const currentCount = parseInt(currentReview.rate_count)
        const currentUsefulValue = parseFloat(currentReview.useful_value)
        const currentFeeValue = parseFloat(currentReview.fee_value)
        
        const newCount = currentCount + 1 
        const newUsefulValue = (currentUsefulValue * currentCount + usefulValue) / newCount
        const newFeeValue = (currentFeeValue * currentCount + feeValue) / newCount
        const newRateValue = (newUsefulValue + newFeeValue) / 2

        let newReview = {
            rate_value: newRateValue, 
            useful_value: newUsefulValue, 
            fee_value: newFeeValue, 
            rate_count: newCount, 
            exist: currentReview.exist
        }

        return resolve(newReview)
    })
}

function didGeniusReviewclazz(geniusId, clazzId) {
    return new Promise(function(resolve, reject) {

        let find = 'SELECT * FROM ' + Tables.dbclazzReviewHistory + ' WHERE clazz_id=' + clazzId + ' AND giver_id=' + geniusId
        database.executeQuery(find)
        .then(
            (foundReview, fields) => {
                if(foundReview.length > 0) {
                    log('reviewed')
                    reject(403)
                }
                else {
                    log('not review yet')
                    resolve(true)
                }
        })
        .catch(
            err => {
                log('err', err)
                reject(500)
        })
    })
}

function fetchReviewOfclazzId(clazzId) {
    return new Promise(function(resolve, reject) {

        const fetch = 'SELECT * FROM ' + Tables.dbclazzReview + ' WHERE clazz_id=' + clazzId
        database.executeQuery(fetch)
        .then(
            (clazzReviews, fields) => {
                if (clazzReviews.length > 0) { 
                    let review = clazzReviews[0]
                    review.exist = true 
                    resolve(review)
                    return
                 }

                 let newReview = { }
                 newReview.rate_count = 0
                 newReview.useful_value = 0 
                 newReview.fee_value = 0 
                 newReview.exist = false
                 resolve(newReview)
                
            })
        .catch(
            err => {
                reject(500)
            })

    })
}

function updateReviewOfclazz(clazzId, rate, useful, fee, count) {

    let update = 'UPDATE ' + Tables.dbclazzReview 
    update    += ' SET rate_value=' + rate + ','
    update    += ' useful_value=' + useful + ','
    update    += ' fee_value=' + fee + ','
    update    += ' rate_count=' + count
    update    += ' WHERE clazz_id=' + clazzId

    log('start updating to review ')
    
    return database.executeQuery(update)
}

function insertReviewOfclazz(clazzId, rate, useful, fee, count) {
    let insert = 'INSERT INTO ' + Tables.dbclazzReview
    insert    += ' (clazz_id, rate_value, useful_value, fee_value, rate_count) VALUES' 
    insert    += ' (' + clazzId + ', ' + rate + ', ' + useful + ', ' + fee + ', ' + count + ')'
    
    log('start insertion to review ')

    return database.executeQuery(insert)
}

function insertIntoReviewHistoryOfclazzFromGeniusComment(clazzId, genius, rateValue, comment) {

    let insert = 'INSERT INTO ' + Tables.dbclazzReviewHistory
    insert    += ' (clazz_id, giver_id, giver_name, giver_profile_image, rate_value, comment, gave_date) VALUES'
    insert    += ' (' + clazzId + ',' + genius.genius_id + ', \"' + genius.genius_name + '\", \"' + genius.genius_profile_image + '\", ' + rateValue + ', \"' + comment + '\", \"' + Date().toString() + '\")'

    log('start insertion to review history ')
    return database.executeQuery(insert)
}









//___________________________________________________ MASTER ________________________________________

function fetchMasterReview(masterId, page) {
    
    var offset = page <= 1 ? 0 : page - 1

    let fetch = 'SELECT * FROM ' + Tables.dbMasterReview + ' review, ' + Tables.dbMasterReviewHistory + ' reviewHistory'
    fetch    += ' WHERE review.master_id=reviewHistory.master_id AND review.master_id=' + masterId 
    fetch    += ' LIMIT ' + itemsOnPage + ' OFFSET ' + offset * itemsOnPage

    let foundReviews
    return new Promise(function(resolve, reject) {

        database.executeQuery(fetch)
        .then(
            (reviews) => {
                foundReviews = reviews
                return countNumberOfMasterReview(masterId)
            })
        .then(
            numberOfReviews => {
                const pagination = {
                    current_page: page <= 0 ? 1 : page, 
                    total_pages: parseInt(numberOfReviews / 5 + 1)
                }

                const returnData = {
                    data: foundReviews,
                    pagination: pagination
                }

                resolve(returnData)
        })
        .catch(
            err => {
                reject(err)
        })
    })
}


function countNumberOfMasterReview(masterId) {
    let countStatement = 'SELECT COUNT(giver_id) as count FROM ' + Tables.dbMasterReviewHistory + ' WHERE master_id=' + masterId
    return new Promise(function(resolve, reject) {

        database.executeQuery(countStatement)
        .then(
            numberOfReview => {
                resolve(numberOfReview[0].count)
        })
    })
    
}


function reviewMaster(genius, masterId, knowledge, inspiration, delivery, comment) {

    let finalReview 

    log('review clazz from genius', genius)
    return new Promise(function(resolve, reject) {

        didGeniusReviewMaster(genius.genius_id, masterId)
        .then(
            didReview => {
                if(didReview == true) {
                    return fetchReviewOfMasterId(masterId)
                }
                reject(403)
            })
        .then(
            review => {
                return calculateMasterReviewFigure(review, knowledge, inspiration, delivery)
            })
        .then(
            newReview => {
                finalReview = newReview

                const geniusRateValue = (newReview.knowledge_value + newReview.inspiration_value + newReview.delivery_value) / 3
                return insertIntoReviewHistoryOfMasterFromGeniusComment(masterId, genius, geniusRateValue, comment)
            })
        .then(
            () => {
                log('clazz review exist', finalReview.exist)
                return finalReview.exist == true ? 
                    updateReviewOfMaster(masterId, finalReview.rate_value, finalReview.knowledge_value, finalReview.inspiration_value, finalReview.delivery_value, finalReview.rate_count) : 
                    insertReviewOfMaster(masterId, finalReview.rate_value, finalReview.knowledge_value, finalReview.inspiration_value, finalReview.delivery_value, finalReview.rate_count)
            })
        .then(
            () => {
                resolve()
            })
        .catch(
            err => {
                log('give review had err ', err)
            })
    })
}

function didGeniusReviewMaster(geniusId, masterId) {
    return new Promise(function(resolve, reject) {

        let find = 'SELECT * FROM ' + Tables.dbMasterReviewHistory + ' WHERE master_id=' + masterId + ' AND giver_id=' + geniusId
        database.executeQuery(find)
        .then(
            (foundReview, fields) => {
                if(foundReview.length > 0) {
                    log('reviewed')
                    reject(403)
                }
                else {
                    log('not review yet')
                    resolve(true)
                }
        })
        .catch(
            err => {
                log('err', err)
                reject(500)
        })
    })
}

function fetchReviewOfMasterId(masterId) {
    return new Promise(function(resolve, reject) {

        const fetch = 'SELECT * FROM ' + Tables.dbMasterReview + ' WHERE master_id=' + masterId
        database.executeQuery(fetch)
        .then(
            (masterReviews, fields) => {
                if (masterReviews.length > 0) { 
                    let review = masterReviews[0]
                    review.exist = true 
                    resolve(review)
                    return
                 }

                 let newReview = { }
                 newReview.rate_count = 0
                 newReview.knowledge_value = 0 
                 newReview.inspiration_value = 0 
                 newReview.delivery_value = 0 
                 newReview.exist = false
                 resolve(newReview)
                
            })
        .catch(
            err => {
                reject(500)
            })

    })
}

function calculateMasterReviewFigure(currentReview, knowledge, inspiration, delivery) {

    return new Promise(function(resolve, reject) {
        const currentCount = parseInt(currentReview.rate_count)
        const currentKnowledgeValue = parseFloat(currentReview.knowledge_value)
        const currentInspirationValue = parseFloat(currentReview.inspiration_value)
        const currentDeliveryValue = parseFloat(currentReview.delivery_value)
        
        log('currentReview', currentReview)

        const newCount = currentCount + 1 
        const newKnowledgeValue = (currentKnowledgeValue * currentCount + knowledge) / newCount
        const newInspirationValue = (currentInspirationValue * currentCount + inspiration) / newCount
        const newDeliveryValue = (currentDeliveryValue * currentCount + delivery) / newCount
        const newRateValue = (newKnowledgeValue + newInspirationValue + newDeliveryValue) / 3

        let newReview = {
            rate_value: newRateValue, 
            knowledge_value: newKnowledgeValue, 
            inspiration_value: newInspirationValue, 
            delivery_value: newDeliveryValue, 
            rate_count: newCount, 
            exist: currentReview.exist
        }
        log('newReview', newReview)
        return resolve(newReview)
    })
}

function insertIntoReviewHistoryOfMasterFromGeniusComment(masterId, genius, rateValue, comment) {

    let insert = 'INSERT INTO ' + Tables.dbMasterReviewHistory
    insert    += ' (master_id, giver_id, giver_name, giver_profile_image, rate_value, comment, gave_date) VALUES'
    insert    += ' (' + masterId + ',' + genius.genius_id + ', \"' + genius.genius_name + '\", \"' + genius.genius_profile_image + '\", ' + rateValue + ', \"' + comment + '\", \"' + Date().toString() + '\")'

    log('start insertion to review history ')
    return database.executeQuery(insert)
}

function updateReviewOfMaster(masterId, rate, knowledge, inspiration, delivery, count) {

    let update = 'UPDATE ' + Tables.dbMasterReview 
    update    += ' SET rate_value=' + rate + ','
    update    += ' knowledge_value=' + knowledge + ','
    update    += ' inspiration_value=' + inspiration + ','
    update    += ' delivery_value=' + delivery + ','
    update    += ' rate_count=' + count
    update    += ' WHERE master_id=' + masterId

    log('start updating to review ')
    
    return database.executeQuery(update)
}

function insertReviewOfMaster(masterId, rate, knowledge, inspiration, delivery, count) {
    let insert = 'INSERT INTO ' + Tables.dbMasterReview
    insert    += ' (master_id, rate_value, knowledge_value, inspiration_value, delivery_value, rate_count) VALUES' 
    insert    += ' (' + masterId + ', ' + rate + ', ' + knowledge + ', ' + inspiration + ', ' + delivery + ', ' + count + ')'
    
    log('start insertion to review ')

    return database.executeQuery(insert)
}








//___________________________________________________ GENIUS ________________________________________

function fetchGeniusReview(geniusId, page) {
    
    var offset = page <= 1 ? 0 : page - 1

    let fetch = 'SELECT * FROM ' + Tables.dbGeniusReview + ' review, ' + Tables.dbGeniusReviewHistory + ' reviewHistory'
    fetch    += ' WHERE review.genius_id=reviewHistory.genius_id AND review.genius_id=' + geniusId 
    fetch    += ' LIMIT ' + itemsOnPage + ' OFFSET ' + offset * itemsOnPage

    let foundReviews
    return new Promise(function(resolve, reject) {

        database.executeQuery(fetch)
        .then(
            (reviews) => {
                foundReviews = reviews
                return countNumberOfGeniusReview(geniusId)
            })
        .then(
            numberOfReviews => {
                const pagination = {
                    current_page: page <= 0 ? 1 : page, 
                    total_pages: parseInt(numberOfReviews / 5 + 1)
                }

                const returnData = {
                    data: foundReviews,
                    pagination: pagination
                }

                resolve(returnData)
        })
        .catch(
            err => {
                reject(err)
        })
    })
}

function countNumberOfGeniusReview(geniusId) {
    let countStatement = 'SELECT COUNT(giver_id) as count FROM ' + Tables.dbGeniusReviewHistory + ' WHERE genius_id=' + geniusId
    return new Promise(function(resolve, reject) {

        database.executeQuery(countStatement)
        .then(
            numberOfReview => {
                resolve(numberOfReview[0].count)
        })
    })
    
}


function reviewGenius(master, geniusId, integrity, curiosity, behaviors, comment) {

    let finalReview 

    log('review clazz from master', master)
    return new Promise(function(resolve, reject) {

        didMasterReviewGenius(master.genius_id, geniusId)
        .then(
            didReview => {
                if(didReview == true) {
                    return fetchReviewOfGeniusId(geniusId)
                }
                reject(403)
            })
        .then(
            review => {
                return calculateGeniusReviewFigure(review, integrity, curiosity, behaviors)
            })
        .then(
            newReview => {
                finalReview = newReview

                const geniusRateValue = (newReview.integrity_value + newReview.curiosity_value + newReview.behaviors_value) / 3
                return insertIntoReviewHistoryOfGeniusFromMasterComment(geniusId, master, geniusRateValue, comment)
            })
        .then(
            () => {
                log('clazz review exist', finalReview.exist)
                return finalReview.exist == true ? 
                    updateReviewOfGenius(geniusId, finalReview.rate_value, finalReview.integrity_value, finalReview.curiosity_value, finalReview.behaviors_value, finalReview.rate_count) : 
                    insertReviewOfGenius(geniusId, finalReview.rate_value, finalReview.integrity_value, finalReview.curiosity_value, finalReview.behaviors_value, finalReview.rate_count)
            })
        .then(
            () => {
                resolve()
            })
        .catch(
            err => {
                log('give review had err ', err)
            })
    })
}

function didMasterReviewGenius(masterId, geniusId) {
    return new Promise(function(resolve, reject) {

        let find = 'SELECT * FROM ' + Tables.dbGeniusReviewHistory + ' WHERE genius_id=' + geniusId + ' AND giver_id=' + masterId
        database.executeQuery(find)
        .then(
            (foundReview, fields) => {
                if(foundReview.length > 0) {
                    log('reviewed')
                    reject(403)
                }
                else {
                    log('not review yet')
                    resolve(true)
                }
        })
        .catch(
            err => {
                log('err', err)
                reject(500)
        })
    })
}

function fetchReviewOfGeniusId(geniusId) {
    return new Promise(function(resolve, reject) {

        const fetch = 'SELECT * FROM ' + Tables.dbGeniusReview + ' WHERE genius_id=' + geniusId
        database.executeQuery(fetch)
        .then(
            (geniusReviews, fields) => {
                if (geniusReviews.length > 0) { 
                    let review = geniusReviews[0]
                    review.exist = true 
                    resolve(review)
                    return
                 }

                 let newReview = { }
                 newReview.rate_count = 0
                 newReview.integrity_value = 0 
                 newReview.curiosity_value = 0 
                 newReview.behaviors_value = 0 
                 newReview.exist = false
                 resolve(newReview)
                
            })
        .catch(
            err => {
                reject(500)
            })

    })
}

function calculateGeniusReviewFigure(currentReview, integrity, curiosity, behaviors) {

    return new Promise(function(resolve, reject) {
        const currentCount = parseInt(currentReview.rate_count)
        const currentIntegrityValue = parseFloat(currentReview.integrity_value)
        const currentCuriosityValue = parseFloat(currentReview.curiosity_value)
        const currentBehaviorsValue = parseFloat(currentReview.behaviors_value)
        
        log('currentReview', currentReview)

        const newCount = currentCount + 1 
        const newIntegrityValue = (currentIntegrityValue * currentCount + integrity) / newCount
        const newCuriosityValue = (currentCuriosityValue * currentCount + curiosity) / newCount
        const newBehaviorsValue = (currentBehaviorsValue * currentCount + behaviors) / newCount
        const newRateValue = (newIntegrityValue + newCuriosityValue + newBehaviorsValue) / 3

        let newReview = {
            rate_value: newRateValue, 
            integrity_value: newIntegrityValue, 
            curiosity_value: newCuriosityValue, 
            behaviors_value: newBehaviorsValue, 
            rate_count: newCount, 
            exist: currentReview.exist
        }
        log('newReview', newReview)
        return resolve(newReview)
    })
}

function insertIntoReviewHistoryOfGeniusFromMasterComment(geniusId, master, rateValue, comment) {

    let insert = 'INSERT INTO ' + Tables.dbGeniusReviewHistory
    insert    += ' (genius_id, giver_id, giver_name, giver_profile_image, rate_value, comment, gave_date) VALUES'
    insert    += ' (' + geniusId + ',' + master.genius_id + ', \"' + master.genius_name + '\", \"' + master.genius_profile_image + '\", ' + rateValue + ', \"' + comment + '\", \"' + Date().toString() + '\")'

    log('start insertion to review history ')
    return database.executeQuery(insert)
}

function updateReviewOfGenius(geniusId, rate, integrity, curiosity, behaviors, count) {

    let update = 'UPDATE ' + Tables.dbGeniusReview 
    update    += ' SET rate_value=' + rate + ','
    update    += ' integrity_value=' + integrity + ','
    update    += ' curiosity_value=' + curiosity + ','
    update    += ' behaviors_value=' + behaviors + ','
    update    += ' rate_count=' + count
    update    += ' WHERE genius_id=' + geniusId

    log('start updating to review ')
    
    return database.executeQuery(update)
}

function insertReviewOfGenius(geniusId, rate, integrity, curiosity, behaviors, count) {
    let insert = 'INSERT INTO ' + Tables.dbGeniusReview
    insert    += ' (genius_id, rate_value, integrity_value, curiosity_value, behaviors_value, rate_count) VALUES' 
    insert    += ' (' + geniusId + ', ' + rate + ', ' + integrity + ', ' + curiosity + ', ' + behaviors + ', ' + count + ')'
    
    log('start insertion to review ')

    return database.executeQuery(insert)
}