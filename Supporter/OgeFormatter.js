module.exports.removeUndefinedFieldsIn = removeUndefinedFieldsIn
module.exports.generateUpdateString = generateUpdateString

function removeUndefinedFieldsIn(genius) {
    for (var property in genius) {
        let value = genius[property]
        if (value == 'null' || value == 'NULL' || value == undefined || value == 'undefined') {
            delete genius[property]
        }
    }

    return genius
}

function generateUpdateString(data) {

    let updateData = removeUndefinedFieldsIn(data)
    let updateString = ''

    for (let key in data) {
        let value = data[key]
        if (typeof(value) == 'number') {
            updateString += key + ' = ' + value
        }
        else {
            updateString += key + ' = \"' + value + '\"'
        }   
        updateString += ','
    }

    updateString = updateString.substring(0, updateString.length - 1)
    return updateString
}