
module.exports.start = start
module.exports.end = end
module.exports.log = log

let geniusId
let geniusName
let action
let startTime
let result
let endTime

function start() {
    this.startTime = new Date()
}

function log(geniusId, geniusName, action) {
    this.geniusId = geniusId
    this.geniusName = geniusName
    this.action = action
}

function end(result, status) {
    this.result = result
    this.endTime = new Date()


    // save to db 
    // log 
    console.log('**************************')
    console.log('Genius:', `${this.geniusId} - ${this.geniusName}`)
    console.log('Action: ', this.action)
    console.log('Start time: ', this.startTime)
    console.log('Result: ', this.result)
    console.log('End time: ', this.endTime)
    console.log('Duration: ', this.endTime - this.startTime)
    console.log('Status: ', status)
    console.log('**************************')
}