//reference path="http://www.restapitutorial.com/httpstatuscodes.html" 

module.exports.response = generateResponse
module.exports.loginNeeded = 'Login need'
module.exports.noPermission = 'No permission'
module.exports.notExist = 'Not exist'
module.exports.notMaster = 'Not a master'
module.exports.notYourclazz = 'Not your clazz'
module.exports.internalError = 'Internal server error'
module.exports.done = 'Done'
module.exports.alreadyReviewed = 'Already given review'
module.exports.notFound = 'Not Found'
module.exports.addYourselfIntoFavouriteMaster = 'You can not add yourself to your favourite master'
module.exports.masterSubmitted = `You've already submitted to become master.`

function generateResponse(code, data, pagination) {

    let returnData = {
        'status_code': code, 
        'data': data
    }
    
    if(pagination != null) {
        returnData.pagination = pagination
    }
    return returnData

    
}