const Config = require('./OgeConfiguration')

module.exports = {
    generateNumberRecordsPagination: function (page) {
        let pageObj = {}
        pageObj.itemEnd = parseInt(page) * Config.itemPerPage
        pageObj.itemStart = pageObj.itemEnd - Config.itemPerPage
        return pageObj
    }
}