
module.exports.addDaysToDate = addDaysToDate

function addDaysToDate(days, date) {
    var result = new Date(date)
    result.setDate(result.getDate() + days)
    return result
}

Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + days)
    return result
}

Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('');
}

Date.prototype.yyyyMMddhhmm = function() {
  var yyyy = this.getFullYear();
   var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
   var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
   var hh = this.getHours() < 10 ? "0" + this.getHours() : this.getHours();
   var min = this.getMinutes() < 10 ? "0" + this.getMinutes() : this.getMinutes();
   return "".concat(yyyy).concat(mm).concat(dd).concat(hh).concat(min);
}

String.prototype.stringToDate = stringToDate

function stringToDate(_date,_format,_delimiter)
{
    var formatLowerCase=_format.toLowerCase();
    var formatItems=formatLowerCase.split(_delimiter);
    var dateItems=_date.split(_delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yyyy");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;
    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    return formatedDate;
}

Date.prototype = yyyyMMddhhmmToDate
function yyyyMMddhhmmToDate(dateString)
{
    let year = dateString.substring(0, 4)
    let month = dateString.substring(4, 6)
    let day = dateString.substring(6, 8)
    let hour = dateString.substring(8, 10)
    let minute = dateString.substring(10, 12)
    month -= 1
    let date = new Date(year, month, day, hour, minute)
    console.log(date)
    return date
}