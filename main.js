var express = require('express')
var app = express()
var cors = require('cors')
var server = require('http').createServer(app)
var routes = require('./routes/index')
require('dotenv').config()

let port = 4000
server.listen(port)

const bodyParser = require('body-parser')
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(routes)

global.database             = require('./DatabaseConnector/OgeDatabaseConnector')
global.logger               = require('./Supporter/OgeLogger')
global.Responser            = require('./Supporter/OgeResponse')
global.config               = require('./Supporter/OgeConfiguration')
global.Tracker              = require('./Supporter/OgeTracker')
global.Formatter            = require('./Supporter/OgeFormatter')

console.log('Server listening on port %s', port)
