## Configuration database
We are using [SequelizeJS ORM](http://docs.sequelizejs.com/) to manage and connect to MySQL server.
So, in `config` folder, please create 2 files called `config.json` and `db.json` with template:

#### config.json
```
{
  "development": {
    "username": "root",
    "password": "ogeniigrowsfast",
    "database": "ogenii_dev",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
```

#### db.json

```
{
  "development": {
    "user": "root",
    "password": "ogeniigrowsfast",
    "database": "ogenii_dev",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "remote": {
    "user": "developer",
    "password": "ogeniigrowsfast",
    "database": "ogenii_dev",
    "host": "128.199.83.140",
    "dialect": "mysql"
  },
  "test": {
    "user": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "user": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
```