const Promise               = require('promise')
const AuthCenter            = require('../Authentication/OgeAuthorizationCenter')
const ResponseComposer      = require('../../Supporter/OgeResponse')
                              require('../../Supporter/OgeLogger')


//___________________________________________________ EXPORTS ________________________________________
module.exports.fetchclazzReview = fetchclazzReview
module.exports.reviewclazz = reviewclazz

module.exports.reviewMaster = reviewMaster
module.exports.fetchMasterReview = fetchMasterReview

module.exports.reviewGenius = reviewGenius
module.exports.fetchGeniusReview = fetchGeniusReview




//___________________________________________________ CONSTANTS ________________________________________

const dbclazzReview = 'clazzes_reviews'
const dbMasterReview = 'masters_reviews'
const dbGeniusReview = 'geniuses_reviews'





//___________________________________________________ REVIEW clazz ________________________________________
async function fetchclazzReview(req, res) {

    let tracker = Tracker
    tracker.start()
    const clazzId = parseInt(req.query.id)
    let page = parseInt(req.query.page)
    page = isNaN(page) ? 0 : page
    
    let reviews = await database.fetchclazzReview(clazzId, page)
    reviews = Formatter.removeUndefinedFieldsIn(reviews)

    let response = Responser.response(200, reviews)
    res.send(response)

    tracker.log(0, 'No track', 'fetchclazzReview')
    tracker.end(response, 'success')
}

function reviewclazz(req, res) {

    //clazz_id: Int
    //useful_point: 1-5
    //fee_point: 1-5
    //comment: String
    
    const clazzId = parseInt(req.body.clazz_id)
    const usefulPoint = parseInt(req.body.useful_point)
    const feePoint = parseInt(req.body.fee_point)
    const comment = req.body.comment

    AuthCenter.authorize(req)
    .then(
        genius => {
            database.reviewclazz(genius, clazzId, usefulPoint, feePoint, comment)
    })
    .then(
        () => {
            let response = ResponseComposer.response(201, { message: ResponseComposer.done })
            res.send(response)
        })
    .catch(
        errCode => {
            let response 
            switch (errCode) {
                case 401:
                    response = ResponseComposer.response(401, { message: ResponseComposer.loginNeeded })
                    break
            
                case 403: 
                    response = ResponseComposer.response(403, { message: ResponseComposer.alreadyReviewed })
                    break
                    
                default:
                    response = ResponseComposer.response(500, { message: ResponseComposer.internalError })
                    break
            }
            res.send(response)        
            
    })
    

}







//___________________________________________________ REVIEW MASTERS ________________________________________


function fetchMasterReview(req, res) {

    const clazzId = parseInt(req.query.id)
    let page = parseInt(req.query.page)
    page = isNaN(page) ? 0 : page
    
    database.fetchMasterReview(clazzId, page)
    .then(
        (returnData) => {
            const response = ResponseComposer.response(200, returnData.data, returnData.pagination)
            res.send(response)
    })
    .catch(
        err => {
            log('err', err)
    })

}


function reviewMaster(req, res) {
    //master_id: Int
    //knowledge_point: 1-5
    //inspiration_point: 1-5
    //delivery_point: 1-5
    //comment: String

    const masterId = parseInt(req.body.master_id)
    const knowledgePoint = parseInt(req.body.knowledge_point)
    const inspirationPoint = parseInt(req.body.inspiration_point)
    const deliveryPoint = parseInt(req.body.delivery_point)
    const comment = req.body.comment

    AuthCenter.authorize(req)
    .then(
        genius => {
            database.reviewMaster(genius, masterId, knowledgePoint, inspirationPoint, deliveryPoint, comment)
    })
    .then(
        () => {
            let response = ResponseComposer.response(201, { message: ResponseComposer.done })
            res.send(response)
        })
    .catch(
        errCode => {
            let response 
            switch (errCode) {
                case 401:
                    response = ResponseComposer.response(401, { message: ResponseComposer.loginNeeded })
                    break
            
                case 403: 
                    response = ResponseComposer.response(403, { message: ResponseComposer.alreadyReviewed })
                    break
                    
                default:
                    response = ResponseComposer.response(500, { message: ResponseComposer.internalError })
                    break
            }
            res.send(response)        
            
    })
}






//___________________________________________________ REVIEW GENIUS ________________________________________

function fetchGeniusReview(req, res) {

    const clazzId = parseInt(req.query.id)
    let page = parseInt(req.query.page)
    page = isNaN(page) ? 0 : page
    
    database.fetchGeniusReview(clazzId, page)
    .then(
        (returnData) => {
            const response = ResponseComposer.response(200, returnData.data, returnData.pagination)
            res.send(response)
    })
    .catch(
        err => {
            log('err', err)
    })

}

function reviewGenius(req, res) {
    //genius_id: Int
    //integrity_point: 1-5
    //curiosity_point: 1-5
    //behaviors_point: 1-5
    //comment: String

    const geniusId = parseInt(req.body.genius_id)
    const integrityPoint = parseInt(req.body.integrity_point)
    const curiosityPoint = parseInt(req.body.curiosity_point)
    const behaviorsPoint = parseInt(req.body.behaviors_point)
    const comment = req.body.comment

    AuthCenter.authorize(req)
    .then(
        master => {
            database.reviewGenius(master, geniusId, integrityPoint, curiosityPoint, behaviorsPoint, comment)
    })
    .then(
        () => {
            let response = ResponseComposer.response(201, { message: ResponseComposer.done })
            res.send(response)
        })
    .catch(
        errCode => {
            let response 
            switch (errCode) {
                case 401:
                    response = ResponseComposer.response(401, { message: ResponseComposer.loginNeeded })
                    break
            
                case 403: 
                    response = ResponseComposer.response(403, { message: ResponseComposer.alreadyReviewed })
                    break
                    
                default:
                    response = ResponseComposer.response(500, { message: ResponseComposer.internalError })
                    break
            }
            res.send(response)        
            
    })
}