let AuthCenter			= require('../Authentication/OgeAuthorizationCenter')
let clazzConstant		= require('../Clazzes/OgeClazzConstants')
let push				= require('../Notification/PushNotification')
let reportDatabase      = require('./OgeErrorReportDatabase')

module.exports.reportErrorSilenly = reportError
module.exports.needHelp = needHelp


async function reportError(req, res) {
    let genius = await AuthCenter.authorize(req)
    let isValidGenius = genius != null
    if (isValidGenius == false) { return }

    let genius_id = genius.genius_id
    let comment = req.body.comment 
    let content = req.body.content 
    let occur_on_action = req.body.occur_on_action
    reportDatabase.reportError(genius_id, occur_on_action, comment, content)
    push.pushReportToAdmin()
}


async function needHelp(req, res) {
    let genius = await AuthCenter.authorize(req)
    let isValidGenius = genius != null
    if (isValidGenius == false) { return }
    let genius_id = genius.genius_id
    let content = req.body.content 
    reportDatabase.needHelp(genius_id, content)
    push.pushNeedHelpToAdmin(genius_id, content)
    let response = Responser.response(200, {message: "Your issue is recorded. We are following up " })
    res.send(response)
}