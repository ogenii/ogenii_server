let clazzConstant         = require('../Clazzes/OgeClazzConstants')
let DateExtension         = require('../../Supporter/DateExtension')
let Formatter             = require('../../Supporter/OgeFormatter')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')

module.exports.reportError = reportError
module.exports.needHelp = needHelp

async function reportError(genius_id, occur_on_action, comment, content) {
    let now = (new Date()).yyyyMMddhhmm()
    let query = `insert into ${Tables.dbErrorReport} (from_genius_id, occur_on_action, comment, content, created_at) values (${genius_id}, "${occur_on_action}", "${comment}", '${content}', "${now}")`
    database.executeQuery(query)
}

async function needHelp(genius_id, content) {
    let now = (new Date()).yyyyMMddhhmm()
    let query = `insert into ${Tables.dbNeedHelp} (from_genius_id, content, created_at) values (${genius_id}, '${content}', "${now}")`
    database.executeQuery(query)
}