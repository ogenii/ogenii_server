let clazzConstant         = require('./../Clazzes/OgeClazzConstants')
let DateExtension         = require('../../Supporter/DateExtension')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let Formatter             = require('../../Supporter/OgeFormatter')
let Push                  = require('../Notification/PushNotification')

module.exports.masterApprovedBooking = masterApprovedBooking
module.exports.masterRejectedBooking = masterRejectedBooking
module.exports.getMastersFromSearchQueries = getMastersFromSearchQueries
module.exports.fetchMasterDetails = fetchMasterDetails
module.exports.addExpertises = addExpertises
module.exports.becomeMaster = becomeMaster
module.exports.fetchTeachingSchedules = fetchTeachingSchedules
module.exports.updateMasterBookingNotification = updateMasterBookingNotification
module.exports.fetchAvailableTime = fetchAvailableTime
module.exports.getRegisteredTeachingSubject = getRegisteredTeachingSubject
module.exports.checkIsMaster = checkIsMaster


function masterApprovedBooking(booking_id) {
    masterResponseBooking(booking_id, clazzConstant.status.upcoming)
}

function masterRejectedBooking(booking_id, reason) {
    masterResponseBooking(booking_id, clazzConstant.status.rejected, reason)
}

async function masterResponseBooking(booking_id, status, reason) {
    let query = ""
    if (reason) {
        query = `update ${Tables.dbLearnSchedules} set status = '${status}', rejected_reason = '${reason}' where booking_id = ${booking_id} `
    }
    else {
        query = `update ${Tables.dbLearnSchedules} set status = '${status}' where booking_id = ${booking_id} `
    }
    
    await database.executeQuery(query)
}

async function updateMasterBookingNotification(pushData) {
    let type = pushData.type
    let action = type == Push.pushTypes.master_approved_booking ? 'approved' : 'rejected'
    let content = `You ${action} Genius ${pushData.genius_name} to join class ${pushData.clazz_title}`

    let query = ""
    if (pushData.rejected_reason != null) {
        content += `. Reason: ${pushData.rejected_reason}`
        query = `update ${Tables.dbNotification} set push_type = '${type}', rejected_reason = '${pushData.rejected_reason}', is_read = true, content = '${content}' where id = ${pushData.notification_id}`
    }
    else {
        query = `update ${Tables.dbNotification} set push_type = '${type}', is_read = true, content = '${content}' where id = ${pushData.notification_id}`
    }
    database.executeQuery(query)
}

// I get Master details in Genius table
async function fetchMasterDetails(masterId) {
   
}

function getMastersFromSearchQueries(keyword, itemRange) {
    return searchMastersByMasterName(keyword, itemRange)
}

async function searchMastersByMasterName(masterName, itemsRange) {
   
    let query = 'SELECT geniuses.full_name master_name, geniuses.genius_id master_id, skills, geniuses.profile_image master_profile_image, rate_value '
    query += 'FROM ' + Tables.dbGeniuses + ' geniuses, masters master '
    query += 'INNER JOIN ' + Tables.dbMasterReview + ' review ON master.master_id = review.master_id '
    query += 'WHERE geniuses.genius_id = master.master_id '
    query += 'and (geniuses.full_name LIKE "%'+ masterName +'%" '
    query += 'or first_name like "%' + masterName + '%" '
    query += 'or last_name like "%' + masterName + '%" '
    query += 'or skills like "%' + masterName + '%" '
    query += 'or about like "%' + masterName + '%")'
    let masters = await database.executeQuery(query)
    masters = masters.length > 0 ? masters : null
    if (masters == null) { return null }

    masters.forEach(function(element) {
        let skills = element.skills
        if (skills !== null) { 
            element.skills = skills.split(', ')
        }
        else {
            element.skills = []
        }
    }, this);
    
    return masters
}


async function addExpertises(data) {
    let today = new Date()
    today = today.yyyyMMddhhmm()

    let query = ""
    for (var index = 0; index < data.length; index++) {
        var item = data[index];
        if (item.sub_category_name) {

            let categoryQuery = `select icon, color_icon from ${Tables.dbCategory} where id = ${item.category_id}`
            let category = await database.executeQuery(categoryQuery)
            if (category.length > 0) { category = category[0] }

            categoryQuery = `insert into ${Tables.dbCategory} (name, belong_to, icon, color_icon) values ("${item.sub_category_name}", ${item.category_id}, '${category.icon}', '${category.color_icon}')`
            let newCategory = await database.executeQuery(categoryQuery)
            item.sub_category_id = newCategory.insertId
        }

        query += `insert into ${Tables.dbExpertise} (master_id, category_id, sub_category_id, started_learning_at, started_teaching_at, proof_type, proof_name, proof_image, created_at) `
        query += `values (${data.master_id}, ${item.category_id}, ${item.sub_category_id}, ${item.started_learning_at}, ${item.started_teaching_at}, '${item.proof_type}', '${item.proof_name}', '${item.proof_image}', '${today}'); `
    }


    let result = await database.executeQuery(query)
    return result != null

}

async function becomeMaster(data) {
    let now = new Date()
    let query = `insert into ${Tables.dbMasters} (master_id, about, open_teaching_time, close_teaching_time, take_weekend_off, teach_night_clazz, take_holiday_off, submitted_at) 
    values (${data.master_id}, "${data.about}", "${data.open_teaching_time}", "${data.close_teaching_time}", "${data.take_weekend_off}", ${data.teach_night_clazz}, ${data.take_holiday_off}, ${now.yyyyMMddhhmm()});`

    let updateQuery = `update ${Tables.dbGeniuses} set full_name = "${data.full_name}", profile_image = "${data.profile_image}" where genius_id = ${data.master_id};`
    if (data.profile_image == null) { updateQuery = '' }

    // let experiences = data.experiences 
    // let experienceInsert = ''
    // experiences.forEach(function(exp) {
    //     experienceInsert = `insert into ${Tables.dbExperience} (master_id, image, title, description) values (${data.master_id}, "${exp.image}", "${exp.title}", "${exp.description}");`
    // }, this);

    let schedule = data.schedules
    let scheduleInsert = `insert into ${Tables.dbSchedules}, (master_id, sun, mon, tue, wed, thu, fri, sat 
        values (${data.master_id}, '${JSON.stringify(schedule.sun)}', '${JSON.stringify(schedule.mon)}', '${JSON.stringify(schedule.tue)}', '${JSON.stringify(schedule.wed)}', '${JSON.stringify(schedule.thu)}', '${JSON.stringify(schedule.fri)}', '${JSON.stringify(schedule.sat)}');`
    query += updateQuery + experienceInsert + scheduleInsert
    let result = await database.executeQuery(query)
    return result    
}


async function fetchTeachingSchedules(master_id) {
    // master_id, master_profile_image, master_name should be genius. but have to set it master to reuse in client 
    let query = `select learn.booking_id, learn.clazz_id, ls.date_time, learn.genius_id master_id, category.icon category_icon , date_time, clazz_title, clazz_duration, location_lat clazz_lat, location_long clazz_long, address clazz_address, genius.full_name genius_name, genius.profile_image genius_profile_image, genius.genius_id, clazz_demo_images 
    from ${Tables.dbLearn} learn, ${Tables.dbClazzes} clazzes, ${Tables.dbCategory} category, ${Tables.dbGeniuses} genius, ${Tables.dbLearnSchedules} ls 
    where clazzes.master_id = ${master_id} and learn.clazz_id = clazzes.clazz_id and genius.genius_id = learn.genius_id and category.id = clazzes.clazz_category and (ls.status = '${clazzConstant.status.upcoming}' or ls.status = '${clazzConstant.status.waiting_for_approval}' or ls.status = '${clazzConstant.status.waiting_for_verification}') and ls.booking_id = learn.booking_id
    `
    let schedules = await database.executeQuery(query)
    schedules = Formatter.removeUndefinedFieldsIn(schedules)
    schedules.forEach(function(element) {
        element.schedules = [element.date_time]
        let photos = element.clazz_demo_images
        photos = JSON.parse(photos)
        element.clazz_demo_images = photos
        element.master = { 'profile_image': element.master_profile_image, 'full_name': element.master_name }
    }, this);

    return schedules
}

function splitTime(time) {
    let elements = (time).split(":")
    return {hour: parseInt(elements[0]), minute: parseInt(elements[1])}
}

function isDateToday(date) {
    const [selected_year, selected_month, selected_day, selected_hour, selected_minute] = getTimeElements(date)  
    let today = new Date().yyyyMMddhhmm()
    const [this_year, this_month, this_day, this_hour, this_minute] = getTimeElements(today)
    let isToday = this_year == selected_year && this_month == selected_month && this_day == selected_day
    return isToday
}

async function fetchAvailableTime(clazz_id, date, dayOfWeek) {

    let master_id = 11
    let query = `select clazz_duration, master_id from ${Tables.dbClazzes} clazz where clazz_id = ${clazz_id}`
    let clazz_detail = await database.executeQuery(query)
    clazz_detail = clazz_detail[0]
    master_id = clazz_detail.master_id
    let this_clazz_duration = clazz_detail.clazz_duration

    query = `select ${dayOfWeek} today_schedule, take_weekend_off, take_holiday_off, teach_night_clazz from ${Tables.dbSchedules} schedule, ${Tables.dbMasters} master where schedule.master_id = ${master_id} and schedule.master_id = master.master_id`
    let master_detail = await database.executeQuery(query)
    master_detail = master_detail[0]
    
    let today_schedule = master_detail.today_schedule
    today_schedule = JSON.parse(today_schedule)
    if (today_schedule.available == "0") { return [] }

    let open = splitTime(today_schedule.open_time)
    let close = splitTime(today_schedule.close_time)
    
    query = `select clazz_duration, date_time booked_schedules from ${Tables.dbClazzes}, ${Tables.dbLearnSchedules} ls, ${Tables.dbLearn} l where clazzes.master_id = ${master_id} and clazzes.clazz_id = l.clazz_id and ls.booking_id = l.booking_id and (ls.status = "${clazzConstant.status.upcoming}" or ls.status = "${clazzConstant.status.waiting_for_approval}" or ls.status = "${clazzConstant.status.waiting_for_verification}")`
    let timeTables = await database.executeQuery(query)

    const [selected_year, selected_month, selected_day, selected_hour, selected_minute] = getTimeElements(date)  
    let isToday = isDateToday(date)
    let this_hour = new Date().getHours()
    let busyTime = []
    for (var timeTable = 0; timeTable < timeTables.length; timeTable++) {
        var element = timeTables[timeTable]
        let booked_schedules = element.booked_schedules
        let duration = parseInt(element.clazz_duration)

        booked_schedules = JSON.parse(booked_schedules)

        for (var booked_schedule = 0; booked_schedule < booked_schedules.length; booked_schedule++) {
            let time = booked_schedules[booked_schedule];
            let [year, month, day, hour, minute] = getTimeElements(time)
            
            let is_same_day = selected_day == day && selected_month == month && selected_year == year 
            if (is_same_day == false) { continue }

            busyTime.push({ hour: hour, minute: minute })

            let quaters = duration / 15
            for (let quarter = 0; quarter < quaters; quarter++) {
                minute += 15 
                if (minute >= 60) { 
                    minute = 0 
                    hour += 1
                }
                busyTime.push({hour: hour, minute: minute})
            }

            [year, month, day, hour, minute] = getTimeElements(time)
            quaters = this_clazz_duration / 15 + 2
            for (let i = 0; i < quaters; i++) {
                minute -= 15 
                if (minute < 0) {
                    minute = 45 
                    hour -= 1
                }

                busyTime.push({hour: hour, minute: minute})
            }
            
        }
    }

    var available_time = []
    close.hour = close.hour < open.hour ? close.hour + 12 : close.hour
    if (isToday && open.hour < this_hour) { open.hour = this_hour + 1 }
    for (var h = open.hour; h < close.hour; h++) {
        for (var m = 0; m <= 60; m += 15) {
            let this_time
            if (m != 60) {
                this_time = { hour: h, minute: m }
                if (doesContain(busyTime, this_time) == false) { 
                    this_time = `${h < 10 ? `0${h}` : h}:${m == 0 ? "00" : m}`
                    available_time.push(this_time) 
                }
            }
            
        }
    }


    return available_time
}


function doesContain(array, item) {
    for (var i = 0; i < array.length; i++) {
        var element = array[i];
        if (element.hour == item.hour && element.minute == item.minute) { 
            return true 
        }
    }
    return false
}

function getTimeElements(dateString) {

    let selected_year = parseInt(dateString.substring(0, 4))
    let selected_month = parseInt(dateString.substring(4, 6))
    let selected_day = parseInt(dateString.substring(6, 8))
    let selected_hour = parseInt(dateString.substring(8, 10))
    let selected_minute = parseInt(dateString.substring(10, 12))
    return [selected_year, selected_month, selected_day, selected_hour, selected_minute]
}

async function getRegisteredTeachingSubject(master_id) {
    let query = `select category.id, name, color_icon icon 
    from ${Tables.dbCategory}, ${Tables.dbExpertise} master_exp
    where category.id = master_exp.sub_category_id and master_exp.master_id = ${master_id} order by category.id  desc`
    let results = await database.executeQuery(query)
    return results
}

async function checkIsMaster(master_id) {
    let query = `select master_id from ${Tables.dbMasters} where master_id = ${master_id} and status = 'approved'`
    let result = await database.executeQuery(query)
    return result.length > 0
}