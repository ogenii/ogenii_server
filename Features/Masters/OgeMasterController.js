let AuthCenter			= require('../Authentication/OgeAuthorizationCenter')
let clazzConstant		= require('../Clazzes/OgeClazzConstants')
let PaginationHelper	= require('../../Supporter/OgePaginationHelpers')
let push				= require('../Notification/PushNotification')
let masterDatabase		= require('./OgeMasterDatabase')
let clazzDatabase 		= require('../Clazzes/OgeClazzDatabase')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')


module.exports.fetchMasters = fetchMasters
module.exports.fetchMasterDetail = fetchMasterDetail
module.exports.searchMaster = searchMaster
module.exports.approveRequestClazz = approveRequestClazz
module.exports.rejectRequestClazz = rejectRequestClazz
module.exports.becomeMaster = becomeMaster
module.exports.addExpertises = addExpertises
module.exports.fetchTeachingSchedules = fetchTeachingSchedules
module.exports.fetchAvailableTime = fetchAvailableTime
module.exports.getRegisteredTeachingSubject = getRegisteredTeachingSubject

function fetchMasters(req, res) {
	
}

async function fetchTeachingSchedules(req, res) {
	
	let master = await AuthCenter.authorize(req)
	let isValidMaster = master != null
    if (isValidMaster == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return false
	}
	
	let schedules = await masterDatabase.fetchTeachingSchedules(master.genius_id)
	schedules.forEach(function(element) {
		element.master = {
			'id': master.genius_id,
			'profile_image': master.profile_image,
			'full_name': master.full_name
		}
	}, this);
	let response = Responser.response(200, schedules)
	res.send(response)
} 

async function fetchMasterDetail(req, res) {

	let masterId = parseInt(req.params.id)
	let foundMaster = await masterDatabase.fetchMasterDetails(masterId)
	if (Object.keys(foundMaster).length > 0) {
		let response = Responser.response(200, { message: foundMaster })
		res.send(response)
	} else {
		let response = Responser.response(404, { message: Responser.notFound })
		res.send(response)
	}
}

function approveRequestClazz(req, res) {
	responseBooking(req, res, clazzConstant.status.approved)
}

async function responseBooking(req, res, action) {
	let bookingId = req.body.booking_id
	let notification_id = req.body.notification_id
	let clazzId = req.body.clazz_id
	let geniusId = req.body.genius_id
	let master = await AuthCenter.authorize(req)

	let isMaster = await masterDatabase.checkIsMaster(master.genius_id)
	if (isMaster == false) { 
		let response = Responser.response(403, { message: Responser.notMaster })
		res.send(response)
		return
	}
	let isOwner = await doesMasterOwnclazz(master.genius_id, clazzId)
	if (isOwner == false) {
		let response = Responser.response(403, { message: Responser.notMaster })
		res.send(response)
		return
	}

	let query = `select device_token token, full_name genius_name, genius_id, clazz_id clazz_id, clazz_title clazz_title 
	from ${Tables.dbGeniuses}, ${Tables.dbClazzes} 
	where genius_id = ${genius_id} and clazz_id = ${clazzId}`
	let pushData = await database.executeQuery(query)
	pushData = pushData.length > 0 ? pushData[0] : null
	if (pushData == null) { return }

	pushData.booking_id = bookingId
	pushData.notification_id = notification_id
	pushData.master_id = master.genius_id 
	pushData.master_name = master.full_name 
	pushData.master_profile_image = master.profile_image

	if(action == clazzConstant.status.approved) {
		pushData.type = push.pushTypes.master_approved_booking
		masterDatabase.masterApprovedBooking(bookingId)
		masterDatabase.updateMasterBookingNotification(pushData)
		let response = Responser.response(200, { message: 'Approved' })
		res.send(response)
		push.pushApprovedMessage(pushData)
		return
	}
	else {
		let rejected_reason = req.body.rejected_reason
		masterDatabase.masterRejectedBooking(bookingId, rejected_reason)
		let response = Responser.response(200, { message: 'Rejected' })
		res.send(response)

		pushData.rejected_reason = rejected_reason
		pushData.type = push.pushTypes.master_rejected_booking
		push.pushRejectedMessage(pushData)
		masterDatabase.updateMasterBookingNotification(pushData)
		return
	}
}

async function doesMasterOwnclazz(masterId, clazzId) {
	let clazz = await clazzDatabase.fetchClazzFromId(clazzId)
	return clazz.master_id == masterId
}


function rejectRequestClazz(req, res) {
	responseBooking(req, res, clazzConstant.declined)	
}

function searchMaster(req, res) {
	let queryString = req.params.query
	let page = (req.params.page) ? ((req.params.page > 1) ? req.params.page : 1) : 1
	let itemRange = PaginationHelper.generateNumberRecordsPagination(page)
	console.log(itemRange)
	if (queryString) {
		masterDatabase.getMastersFromSearchQueries(queryString, itemRange).then((foundMasters) => {
			let response = (foundMasters.length > 0) ? 
							Responser.response(200, foundMasters, page) : 
							Responser.response(404, { message: Responser.notFound })
			res.send(response)
		})
	} else {
		res.send(Responser.response(404, { message: 'Not Found' }))
	}
}

async function addExpertises(req, res) {

	let master = await AuthCenter.authorize(req)
    let isValidMaster = master != null
    if (isValidMaster == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return false
    }

	let expertises = req.body.expertises
	expertises.master_id = master.genius_id
	let isSuccess = await masterDatabase.addExpertises(expertises)
	if (isSuccess == true) {
		let response = Responser.response(201, { message: 'Add success' })
		res.send(response)
	}
	else {
		let response = Responser.response(500, { message: Responser.internalError })
		res.send(response)
	}
}


async function becomeMaster(req, res) {

	let master = await AuthCenter.authorize(req)
	let isValid = master != null 
	
	if (isValid == false) { 
		let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
		return
	}

	let data = req.body
	data.master_id = master.genius_id

	let result = {}
	try {
		result = await masterDatabase.becomeMaster(data)	
	} catch (error) {
		let response = Responser.response(500, { message: 'Something went wrong. We will get back to you soon.' })	
		res.send(response)
		return
	}
	
	if (result.error == true) { 
		let response = Responser.response(result.code, { message: result.message })
		res.send(response)
		return
	}

	let isSuccess = true 
	result.forEach(function(element) {
		isSuccess = isSuccess && element.affectedRows == 1
	}, this);

	if (isSuccess) {
		let response = Responser.response(201, { message: 'Your submission is in review. We will get back to you soon.'})
		res.send(response)
		push.pushNewMasterToAdmin()
	}
	else {
		let response = Responser.response(500, { message: 'Something went wrong. We will get back to you soon.' })
		res.send(response)
	}
	
}

async function fetchAvailableTime(req, res) {
	let master = await AuthCenter.authorize(req)
	let isValid = master != null 
	if (isValid == false) { 
		let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
		return
	}

	let clazz_id = req.query.clazz_id
	let date = req.query.date
	let day_of_week = req.query.day_of_week
	let availableTimeSlots = await masterDatabase.fetchAvailableTime(clazz_id, date, day_of_week)
	let response = Responser.response(200, availableTimeSlots)
	res.send(response)
}

async function getRegisteredTeachingSubject(req, res) {
	let master = await AuthCenter.authorize(req)
	let isValid = master != null 
	if (isValid == false) { 
		let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
		return
	}

	let registeredSubjects = await masterDatabase.getRegisteredTeachingSubject(master.genius_id)
	let response = Responser.response(200, registeredSubjects)
	res.send(response)	
}

