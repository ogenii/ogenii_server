let clazzConstant         = require('./../Clazzes/OgeClazzConstants')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let Formatter             = require('../../Supporter/OgeFormatter')

module.exports.fetchPendingClazzes = fetchPendingClazzes
module.exports.approveClazz = approveClazz
module.exports.rejectClazz = rejectClazz
module.exports.fetchVerificationsPending = fetchVerificationsPending
module.exports.fetchSpecificVerification = fetchSpecificVerification
module.exports.approveVerification = approveVerification
module.exports.rejectVerification = rejectVerification
module.exports.fetchWaitingForVerificationClazzes = fetchWaitingForVerificationClazzes
module.exports.updateWaitingForVerificationToWaitingForApproval = updateWaitingForVerificationToWaitingForApproval
module.exports.fetchPendingMaster = fetchPendingMaster
module.exports.fetchSpecificPendingMaster = fetchSpecificPendingMaster
module.exports.approveMaster = approveMaster
module.exports.rejectMaster = rejectMaster
module.exports.checkIsAdmin = checkIsAdmin



async function fetchPendingClazzes() {
    let query = `select clazz_id, clazz_title, full_name master_name, created_at from ${Tables.dbClazzes} clazz, ${Tables.dbGeniuses} genius where master_id = genius_id and status = '${clazzConstant.status.pending}'`
    let result = await database.executeQuery(query)
    return result
}

async function approveClazz(clazz_id, admin_id) {
    let now = (new Date()).yyyyMMddhhmm()
    let query = `update ${Tables.dbClazzes} set status = '${clazzConstant.status.approved}', responsed_by = ${admin_id}, responsed_at = '${now}' where clazz_id = ${clazz_id}`
    let result = await database.executeQuery(query)
    return result.affectedRows > 0
}

async function rejectClazz(clazz_id, admin_id, reason) {
    let now = (new Date()).yyyyMMddhhmm()
    let query = `update ${Tables.dbClazzes} set status = '${clazzConstant.status.rejected}', responsed_by = ${admin_id}, responsed_at = '${now}', rejected_reason = "${reason}"  where clazz_id = ${clazz_id} `
    let result = await database.executeQuery(query)
    return result.affectedRows > 0
}

async function fetchSpecificVerification(genius_id) {
    let query = `select verify.full_name, verify.birthdate, verify.genius_id, document_photo, selfie_photo, verify.profile_image, email, phone_number from ${Tables.dbGeniusVerification} verify, ${Tables.dbGeniuses} genius where verify.genius_id = ${genius_id} and verify.genius_id = genius.genius_id    `
    let data = await database.executeQuery(query)
    data = data[0]
    return data 
} 

async function fetchVerificationsPending() {
    let query = `select g.genius_id, g.full_name 
    from ${Tables.dbGeniuses} g, ${Tables.dbGeniusVerification} gv
    where g.genius_id = gv.genius_id and (gv.status = 'pending' or gv.status = null)`
    let data = await database.executeQuery(query)
    return data 
}


async function approveVerification(genius_id, admin_id) {
    let now = (new Date()).yyyyMMddhhmm()
    query = `update ${Tables.dbGeniusVerification} set responsed_at = ${now}, responsed_by = ${admin_id}, status = '${clazzConstant.verification_status.approved}' where genius_id = ${genius_id}`
    let result = await database.executeQuery(query)
    return result.affectedRows > 0
}

async function rejectVerification(genius_id, admin_id, reason) {
    let now = (new Date()).yyyyMMddhhmm()
    query = `update ${Tables.dbGeniusVerification} set responsed_at = ${now}, responsed_by = ${admin_id}, status = '${clazzConstant.verification_status.rejected}', rejected_reason = '${reason}' where genius_id = ${genius_id}`
    let result = await database.executeQuery(query)
    return result.affectedRows > 0
}

async function fetchWaitingForVerificationClazzes(genius_id) {
    let query = `select learn.genius_id, master_id, clazz_title, learn.clazz_id, ls.booking_id, full_name 
    from ${Tables.dbGeniuses} geniuses, ${Tables.dbLearn} learn, ${Tables.dbClazzes} clazzes, ${Tables.dbLearnSchedules} ls
    where learn.genius_id = ${genius_id} and learn.genius_id = geniuses.genius_id and clazzes.clazz_id = learn.clazz_id and ls.status = '${clazzConstant.status.waiting_for_verification}' and ls.booking_id = learn.booking_id`
    let data = await database.executeQuery(query)
    return data 
}


async function updateWaitingForVerificationToWaitingForApproval(genius_id) {
    let query = `update ${Tables.dbLearnSchedules} ls set status = '${clazzConstant.status.waiting_for_approval}' where booking_id in (select booking_id from ${Tables.dbLearn} where genius_id = ${genius_id})`
    database.executeQuery(query)
}

async function fetchPendingMaster() {
    let query = `select full_name, master.master_id from ${Tables.dbMasters} master, ${Tables.dbGeniuses} genius where master.master_id = genius.genius_id and master.status = "${clazzConstant.master_status.pending}"`
    return await database.executeQuery(query)
}

async function fetchSpecificPendingMaster(master_id) {
    let query = `select genius.full_name, document_photo, genius.profile_image, selfie_photo, about 
    from ${Tables.dbGeniuses} genius, ${Tables.dbGeniusVerification} verify, ${Tables.dbMasters} master
    where genius.genius_id = verify.genius_id and genius.genius_id = ${master_id} and master.master_id = genius.genius_id;`
    let master = await database.executeQuery(query)
    master = master[0]
    master.master_id = master_id

    query = `select master_id, sub_category_id category_id, category.name category_name, category.icon category_id, started_learning_at, started_teaching_at, proof_type, proof_name, proof_image 
    from ${Tables.dbExpertise} exp, ${Tables.dbCategory} category 
    where exp.master_id = ${master_id} and category.id = sub_category_id;`
    let expertise = await database.executeQuery(query)
    master.expertise = expertise

    query = `select master_id, image certificate_image, title certificate_name, description 
    from ${Tables.dbExperience} 
    where master_id = ${master_id}`
    let experience = await database.executeQuery(query)
    master.experience = experience

    return master
}

async function approveMaster(master_id, approver) {
    let now = (new Date()).yyyyMMddhhmm()
    let query = `update ${Tables.dbMasters} set status = "${clazzConstant.master_status.approved}", responsed_at = "${now}", responsed_by = ${approver} where master_id = ${master_id}`
    let result = await database.executeQuery(query)
    let isSuccess = result.affectedRows == 1
    return isSuccess
}

async function rejectMaster(master_id, admin_id, reason) {
    let now = (new Date()).yyyyMMddhhmm()
    query = `update ${Tables.dbMasters} set responsed_at = ${now}, responsed_by = ${admin_id}, status = '${clazzConstant.master_status.rejected}', rejected_reason = '${reason}' where master_id = ${master_id}`
    let result = await database.executeQuery(query)
    return result.affectedRows > 0
}


async function countAdminNotification(master_id) {
    let query = ``
}

async function checkIsAdmin(admin_id) {
    let query = `select genius_id from ${Tables.dbAdmin} where genius_id = ${admin_id}`
    let result = await database.executeQuery(query)
    return result.length > 0
}