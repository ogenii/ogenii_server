let AuthCenter			= require('../Authentication/OgeAuthorizationCenter')
let clazzConstant		= require('../Clazzes/OgeClazzConstants')
let push				= require('../Notification/PushNotification')
let adminDatabase		= require('./OgeAdminDatabase')

module.exports.fetchPendingClazzes = fetchPendingClazzes
module.exports.approveClazz = approveClazz
module.exports.rejectClazz = rejectClazz
module.exports.fetchVerificationsPending = fetchVerificationsPending
module.exports.fetchSpecificVerification = fetchSpecificVerification
module.exports.approveVerification = approveVerification
module.exports.rejectVerification = rejectVerification
module.exports.fetchPendingMaster = fetchPendingMaster
module.exports.fetchSpecificPendingMaster = fetchSpecificPendingMaster
module.exports.approveMaster = approveMaster
module.exports.rejectMaster = rejectMaster
module.exports.testPush = testPush


async function fetchPendingClazzes(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }
    
    let pending_clazzes = await adminDatabase.fetchPendingClazzes()
    let response = Responser.response(200, pending_clazzes)
	res.send(response)
}

async function approveClazz(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let clazz_id = req.body.clazz_id
    let admin_id = admin.genius_id
    let result = await adminDatabase.approveClazz(clazz_id, admin_id)
    if (result == true) { 
        push.pushAdminApproveClazz(clazz_id, admin_id)        
        res.send(Responser.response(200, {message: "Approved"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
    
}

async function testPush(req, res) {
    // push.pushNewVerificationToAdmin()

    res.send("alo. ")
}

async function checkAdmin(req, res) {
    let admin = await AuthCenter.authorize(req)
    let isAdmin = await adminDatabase.checkIsAdmin(admin.genius_id)
	let isValidAdmin = admin != null && isAdmin == true 
    if (isValidAdmin == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return null
    }
    return admin
}

async function rejectClazz(req, res) {
    
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }
    
    let clazz_id = req.body.clazz_id
    let reason = req.body.reason
    let admin_id = admin.genius_id

    if (reason == null || reason == "") { 
        res.send(Responser.response(400, {message: "Reason is needed" }))
        return 
    }

    let result = await adminDatabase.rejectClazz(clazz_id, admin_id, reason)
    if (result == true) { 
        push.pushAdminRejectClazz(clazz_id, admin_id, reason)
        res.send(Responser.response(200, {message: "Rejected"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
}

async function fetchVerificationsPending(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let result = await adminDatabase.fetchVerificationsPending()
    let response = Responser.response(200, result)
	res.send(response)
}

async function fetchSpecificVerification(req, res) { 
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let genius_id = req.params.genius_id
    let result = await adminDatabase.fetchSpecificVerification(genius_id)
    let response = Responser.response(200, result)
	res.send(response)
}

async function approveVerification(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let genius_id = req.body.genius_id
    let admin_id = admin.genius_id
    let result = await adminDatabase.approveVerification(genius_id, admin_id)
    if (result == true) { 
        push.pushAdminApproveVerification(genius_id, admin_id)
        pushWaitingForVerificationClazzes(genius_id)
        adminDatabase.updateWaitingForVerificationToWaitingForApproval(genius_id)
        res.send(Responser.response(200, {message: "Approved"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
}

async function pushWaitingForVerificationClazzes(genius_id) {
    let pendingClazzes = await adminDatabase.fetchWaitingForVerificationClazzes(genius_id)
    for (var index = 0; index < pendingClazzes.length; index++) {
        var data = pendingClazzes[index];
        push.pushBookedMessage(data)
    }
}

async function rejectVerification(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let genius_id = req.body.genius_id
    let reason = req.body.reason
    let admin_id = admin.genius_id
    let result = await adminDatabase.rejectVerification(genius_id, admin_id, reason)
    if (result == true) { 
        push.pushAdminRejectVerification(genius_id, admin_id, reason)
        res.send(Responser.response(200, {message: "Rejected"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
}

async function fetchPendingMaster(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let result = await adminDatabase.fetchPendingMaster()
    let response = Responser.response(200, result)
	res.send(response)
}

async function fetchSpecificPendingMaster(req, res) {
	let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let master_id = req.params.master_id
    let result = await adminDatabase.fetchSpecificPendingMaster(master_id)
    let response = Responser.response(200, result)
	res.send(response)
}


async function approveMaster(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let master_id = req.body.master_id
    let admin_id = admin.genius_id
    let result = await adminDatabase.approveMaster(master_id, admin_id)
    if (result == true) { 
        push.pushAdminApproveMaster(master_id, admin_id)
        res.send(Responser.response(200, {message: "Approved"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
}

async function rejectMaster(req, res) {
    let admin = await checkAdmin(req, res)
    if (admin == null) { return }

    let master_id = req.body.master_id
    let reason = req.body.reason
    let admin_id = admin.genius_id
    let result = await adminDatabase.rejectMaster(master_id, admin_id, reason)
    if (result == true) { 
        push.pushAdminRejectMaster(master_id, admin_id, reason)
        res.send(Responser.response(200, {message: "Rejected"})) 
    }
    else { res.send(Responser.response(500, {message: "Not success"})) }
}