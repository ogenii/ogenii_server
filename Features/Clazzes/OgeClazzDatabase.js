let moment  = require('moment')
let clazzConstant         = require('./../Clazzes/OgeClazzConstants')
let Tables  = require('../../DatabaseConnector/OgeDatabaseTables')

module.exports.fetchClazzDetail = fetchClazzDetail 
module.exports.discoverClazzes = discoverClazzes
module.exports.fetchClazzFromId = fetchClazzFromId
module.exports.fetchClazzFaq = fetchClazzFaq
module.exports.updateViewCountOfClazz = updateViewCountOfClazz
module.exports.getClazzesFromSearchQueries = getClazzesFromSearchQueries
module.exports.fetchClazzReceipt = fetchClazzReceipt
module.exports.getClazzesStartInADay = getClazzesStartInADay
module.exports.getGeniusJoined = getGeniusJoined
module.exports.fetchClazzReview = fetchClazzReview
module.exports.fetchTopViewClazzes = fetchTopViewClazzes
module.exports.fetchClazzesInCategory = fetchClazzesInCategory
module.exports.fetchClazzes = fetchClazzes
module.exports.createClazz = createClazz
module.exports.addClazzRoom = addClazzRoom
module.exports.fetchClazzRooms = fetchClazzRooms
module.exports.fetchSubcategoryInCategory = fetchSubcategoryInCategory
module.exports.fetchLiveClazzes = fetchLiveClazzes

async function fetchClazzReceipt(session_id) {

    let query = `select c.clazz_description, clazz_title, clazz_demo_images, category.name category_name, category.icon category_icon, category.id category_id, l.book_mode, l.clazz_id, c.master_id, c.clazz_price, c.currency, c.clazz_note, c.clazz_duration from ${Tables.dbClazzes} c, ${Tables.dbLearn} l, ${Tables.dbCategory} category, ${Tables.dbLearnSchedules} ls where ls.session_id = ${session_id} and category.id = c.clazz_category and c.clazz_id = l.clazz_id and l.booking_id = ls.booking_id`
    let clazz = await database.executeQuery(query)
    clazz = clazz.length > 0 ? clazz[0] : null

    if (clazz == null) { return null }

    query = `select address clazz_address, location_lat, location_long, date_time from ${Tables.dbLearnSchedules} where session_id = ${session_id} and (status = '${clazzConstant.status.approved}' or status = '${clazzConstant.status.waiting_for_approval}' or status = '${clazzConstant.status.waiting_for_verification}' or status = '${clazzConstant.status.completed}' or status = '${clazzConstant.status.upcoming}')`
    let result = await database.executeQuery(query)
    clazz.schedules = result

    let photos = clazz.clazz_demo_images
    clazz.clazz_demo_images = JSON.parse(photos)
    clazz = Formatter.removeUndefinedFieldsIn(clazz)

    query = `select profile_image, full_name, about from ${Tables.dbMasters} master, ${Tables.dbGeniuses} genius  where master.master_id = ${clazz.master_id}  and master.master_id=genius.genius_id`
    let master = await database.executeQuery(query)
    master = master.length > 0 ? master[0] : null
    clazz.master = Formatter.removeUndefinedFieldsIn(master)
    clazz.master.master_id = clazz.master_id
    delete clazz.master_id

    query = `select image from ${Tables.dbExperience} where master_id = ${clazz.master.master_id}`
    let master_images = await database.executeQuery(query)
    if(master_images.length > 0) {
        master_images = master_images[0].image
        master_images = master_images.split(',')
        clazz.master.images = master_images
    }

    query = `select clazz_price, currency, clazz_demo_images, clazz_title, profile_image master_profile_image, full_name master_name from ${Tables.dbClazzes} c, ${Tables.dbGeniuses} g where clazz_category = ${clazz.category_id} and c.master_id = g.genius_id and !(c.clazz_id = ${clazz.clazz_id})`
    let similarClazzes = await database.executeQuery(query)

    similarClazzes.forEach(function(element) {
        
        let clazz_images = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(clazz_images)
        element = Formatter.removeUndefinedFieldsIn(element)

    }, this);

    clazz.similar_clazzes = similarClazzes

    query = 'select question, answer from ' + Tables.dbClazzFaq + ' where clazz_id=' + clazz.clazz_id + ' '
    query += 'ORDER BY id ASC LIMIT 1'
    let faq = await database.executeQuery(query)
    faq = faq.length > 0 ? faq[0] : null
    clazz.faq = faq

    return clazz
    
}


async function discoverClazzes() {
    let query = `select clazz_category category_id, name category_name, icon category_icon, clazz_demo_images, clazz_title, clazz_id, clazz_description, profile_image master_profile_image, full_name master_name, master_id, created_at, updated_at from ${Tables.dbClazzes} clazz, ${Tables.dbGeniuses} genius, ${Tables.dbCategory} `
    query += `where clazz.master_id=genius.genius_id and clazz_category=id and clazz.status = '${clazzConstant.status.approved}' `
    query += 'ORDER BY view_count DESC'
    let clazzes = await database.executeQuery(query)

    clazzes.forEach(function(element) {
        let photos = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(photos)

        element = Formatter.removeUndefinedFieldsIn(element)
    }, this);

    return clazzes
}


async function fetchClazzDetail(clazz_id) {

    let query = `select c.clazz_id, c.clazz_title, c.clazz_price, c.currency, c.clazz_duration, c.level, c.clazz_type, c.clazz_description, c.clazz_demo_images, c.clazz_images_description, c.clazz_rating, c.master_id, c.min_session, c.created_at, c.updated_at, c.teach_area, c.clazz_rooms, ca.name category_name, ca.icon category_icon, ca.id category_id
    from ${Tables.dbClazzes} c, ${Tables.dbCategory} ca
    where clazz_id = ${clazz_id} and ca.id = c.clazz_category`


    // let query = 'select clazz.*, category.name category_name, category.icon category_icon, category.id category_id, clazz_rooms '
    // query += 'from ' + Tables.dbClazzes + ' clazz '
    // query += 'join ' + Tables.dbCategory + ' category '
    // query += 'on clazz.clazz_category = category.id '
    // query += 'where clazz_id = ' + clazzId + ' and id = clazz_category'
    let clazz = await database.executeQuery(query)

    if (clazz.length == 0) { return null }
    clazz = clazz[0]
    let master_id = clazz.master_id    
    let clazz_images = clazz.clazz_demo_images
    clazz.clazz_demo_images = JSON.parse(clazz_images)
    
    query = `select * from ${Tables.dbClazzRoom} where room_id = ${clazz.clazz_rooms}`
    let room = await database.executeQuery(query)
    if (room.length > 0) { room = room[0] }
    clazz.clazz_address = room.address
    clazz.clazz_lat = room.latitude
    clazz.clazz_long = room.longitude
    delete clazz.clazz_rooms


    query = `select question, answer from ${Tables.dbClazzFaq} where clazz_id = ${clazz_id} order by id asc limit 1`
    let faq = await database.executeQuery(query)
    faq = faq.length > 0 ? faq[0] : null
    clazz.faq = faq
    


    query = `select h.clazz_id, count(h.rate_value) as count, h.rate_value, h.comment, h.gave_date, r.rate_value general_value 
    from ${Tables.dbClazzReviewHistory} h, ${Tables.dbClazzReview} r
    where h.clazz_id = ${clazz_id} and h.clazz_id = r.clazz_id 
    group by h.clazz_id, h.gave_date, h.rate_value, h.comment 
    order by h.gave_date asc limit 1`


    // query = 'select history.clazz_id, count(history.rate_value) as count, history.rate_value, history.comment, history.gave_date, review.rate_value general_value '
    // query += 'from ' + Tables.dbClazzReviewHistory + ' history, ' + Tables.dbClazzReview + ' review '
    // query += 'where history.clazz_id = ' + clazz_id + ' and history.clazz_id = review.clazz_id '
    // query += 'group by history.clazz_id, history.gave_date, history.rate_value, history.comment, history.gave_date '
    // query += 'ORDER BY history.gave_date ASC LIMIT 1 '
    let review = await database.executeQuery(query)
    review = review.length > 0 ? review[0] : null
    clazz.review = review
    

    query = `select master_id, profile_image, full_name, about 
    from ${Tables.dbMasters} master, ${Tables.dbGeniuses} 
    where master.master_id = ${master_id} and master_id = genius_id`
    let master = await database.executeQuery(query)
    master = master.length > 0 ? master[0] : null
    clazz.master = master
    delete clazz.master_id

    query = `select proof_image image from ${Tables.dbExpertise} where master_id = ${master_id}`
    let master_images = await database.executeQuery(query)
    let all_images = ""
    master_images = master_images.map(element => element.image)
    if (clazz.master != null ) { clazz.master.images = master_images }
    clazz.master = Formatter.removeUndefinedFieldsIn(master)    


    query = `select clazz_price, currency, clazz_demo_images, clazz_title, profile_image master_profile_image, full_name master_name, clazz_id 
    from ${Tables.dbClazzes} clazz, ${Tables.dbGeniuses} genius 
    where (clazz_category = ${clazz.category_id} and clazz.master_id = genius.genius_id) and status = '${clazzConstant.status.approved}' and !(clazz.clazz_id = ${clazz_id})`
    let similarClazzes = await database.executeQuery(query)

    similarClazzes.forEach(function(element) {
        let clazz_images = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(clazz_images)
        element = Formatter.removeUndefinedFieldsIn(element)
    }, this);
    clazz.similar_clazzes = similarClazzes

    
    return clazz
}


function editSpaceImagesFromClazz(foundClazz) {
    foundClazz.clazz_space_images = parseJsonFromRawString(foundClazz.clazz_space_images)
}


function parseJsonFromRawString(rawString) {

    if(rawString == null) { return [] }
    try         {   return JSON.parse(rawString)    } 
    catch(e)    {   return []     }
}


function editDemoImagesFromClazz(foundClazz) {
    foundClazz.clazz_demo_images = parseJsonFromRawString(foundClazz.clazz_demo_images)
}


function editClazzFaciltiesFromClazz(foundClazz) {
    foundClazz.clazz_space_facilities = parseJsonFromRawString(foundClazz.clazz_space_facilities)
}


function editBenefitsFromClazz(foundClazz) {
    foundClazz.clazz_benefits = parseJsonFromRawString(foundClazz.clazz_benefits)
}


function editMasterCoverImagesFromClazz(foundClazz) {
    foundClazz.master_cover_images = parseJsonFromRawString(foundClazz.master_cover_images)
}


function editMasterFacilitiesFromClazz(foundClazz) {
    foundClazz.master_facilities = parseJsonFromRawString(foundClazz.master_facilities)
}


function editMasterExperienceFromClazz(foundClazz) {
    foundClazz.master_experience_tags = parseJsonFromRawString(foundClazz.master_experience_tags)
}


async function updateViewCountOfClazz(clazzId) {

    let query = 'SELECT view_count FROM ' + Tables.dbClazzes + ' WHERE clazz_id=' + clazzId
    let viewCount = await database.executeQuery(query)
    if (viewCount.length > 0) { viewCount = parseInt(viewCount[0].view_count) + 1 }
    else { viewCount = 1 }

    query = 'UPDATE ' + Tables.dbClazzes + ' SET view_count=' + viewCount + ' WHERE clazz_id=' + clazzId
    database.executeQuery(query)
}


async function fetchClazzFromId(clazzId) {

    let query = 'SELECT * FROM ' + Tables.dbClazzes + ' WHERE clazz_id=' + clazzId
    let clazzes = await database.executeQuery(query)
    if (clazzes.length > 0) { return clazzes[0] }
    return null
}


async function fetchClazzFaq(clazzId) {
    let fetch = 'SELECT * FROM ' + Tables.dbClazzFaq + ' WHERE clazz_id=' + clazzId
    let faqs = await database.executeQuery(query)
    return faqs
}


async function getClazzesFromSearchQueries(keyword) {
    let query = 'SELECT icon category_icon, name category_name, clazz_title, clazz_id, clazz_demo_images '
    query += 'FROM ' + Tables.dbClazzes + ' clazz, ' + Tables.dbCategory + ' category '
    query += ' WHERE clazz_category = category.id and '
    query += ' (clazz_title LIKE "%'+ keyword +'%" or '
    query += ' clazz_description LIKE "%'+ keyword +'%" or '
    query += ` clazz_note LIKE "%'+ keyword +'%") and status = '${clazzConstant.status.approved}'  `
    let clazzes = await database.executeQuery(query)
    clazzes = clazzes.length > 0 ? clazzes : null

    if (clazzes == null) { return null }

    clazzes.forEach(function(element) {
        
        let photos = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(photos)

    }, this);

    return clazzes
}


async function fetchClazzes(categoryId) {
    let query = `select clazzes.*, name category_name, icon category_icon from ${Tables.dbCategory} category, ${Tables.dbClazzes} clazzes where clazzes.clazz_category = category.id where status = '${clazzConstant.status.approved}' `
    let clazzes = await database.executeQuery(query)

    if (clazzes == null) { return null }
    clazzes.forEach(function(element) {
        
        let photos = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(photos)

    }, this);

    return clazzes
}


async function fetchClazzesInCategory(categoryId) {
    let query = 'SELECT icon category_icon, name category_name, clazz_title, clazz_id, clazz_demo_images '
    query += `FROM ${Tables.dbClazzes} clazz, ${Tables.dbCategory} category `
    query += ` WHERE clazz_category = ${categoryId} and id = ${categoryId} and status = '${clazzConstant.status.approved}'  `

    let clazzes = await database.executeQuery(query)
    clazzes = clazzes.length > 0 ? clazzes : null

    if (clazzes == null) { return null }
    clazzes.forEach(function(element) {
        
        let photos = element.clazz_demo_images
        element.clazz_demo_images = JSON.parse(photos)

    }, this);

    return clazzes
}


function getGeniusJoined(clazzId, masterId) {
    const query = 'SELECT genius_id FROM ' + Tables.dbRequest + 'WHERE clazz_id = ' + clazzId + ' AND masterId = ' + masterId

    return new Promise(function (resolve, reject) {
        database.executeQuery(query).then((res) => {
            resolve(res)
        })
    })
}


function getClazzesStartInADay() {
    let presentTimestamp = moment().unix()
    let afterADayTimestamp = moment(presentTimestamp).add('1', 'days').unix()
    let query = 'SELECT clazz_id, clazz_title, master_id FROM ' + Tables.dbClazzes + ' WHERE created_day BETWEEN FROM_UNIXTIME(' + presentTimestamp + ') AND FROM_UNIXTIME(' + afterADayTimestamp + ')'

    return new Promise(function (resolve, reject) {
        database.executeQuery(query).then((clazzes, fields) => {
            console.log('Searched clazzes start in a day')
            resolve(clazzes)
        })
    })
}


async function fetchClazzReview(clazzId) {
    
    let query = 'SELECT gave_date, rate_value, comment '
    query += 'FROM ' + Tables.dbClazzReviewHistory + ' '
    query += ' WHERE clazz_id = ' + parseInt(clazzId) + ' ORDER BY gave_date DESC LIMIT 10'
    let history = await database.executeQuery(query)

    query = 'select rate_value, useful_value, fee_value from ' + Tables.dbClazzReview  + ' '
    query += 'where clazz_id=' + clazzId
    let overview = await database.executeQuery(query)
    overview = overview.length > 0 ? overview[0] : null

    query = 'select rate_value, knowledge_value, inspiration_value, delivery_value '
    query += 'from ' + Tables.dbMasterReview + ' master, ' + Tables.dbClazzes + ' clazz '
    query += 'where clazz.master_id = master.master_id and clazz_id = ' + clazzId
    let masterReview = await database.executeQuery(query)
    masterReview = masterReview.length > 0 ? masterReview[0] : null

    return { 'history': history, 'overview': overview, 'master': masterReview }
}


async function fetchTopViewClazzes(itemsRange) {
    const query = 'SELECT * FROM ' + Tables.dbClazzes + ' ORDER BY view_count DESC LIMIT ' + itemsRange.itemStart + ', ' + itemsRange.itemEnd
    let clazzes = await database.executeQuery(query)
    return clazzes
}

async function addClazzRoom(data) {

    data.images = JSON.stringify(data.images)
    let query = `insert into ${Tables.dbClazzRoom} (name, address, latitude, longitude, images, description, belong_to_master) `
    query += `values ("${data.name}", "${data.address}", ${data.latitude}, ${data.longitude}, '${data.images}', "${data.description}", ${data.belong_to_master})`
    let roomId = await database.executeQuery(query)
    return roomId
}


async function fetchClazzRooms(masterId) {
    let query = `select * from ${Tables.dbClazzRoom} where belong_to_master = ${masterId}`
    let result = await database.executeQuery(query)

    result.forEach(async function(element) {
        element.images = JSON.parse(element.images)
    }, this);

    return result
}

async function createClazz(data) {

    let query = ""

    if (data.clazz_sub_category_name) {
        let categoryQuery = `select icon, color_icon from ${Tables.dbCategory} where id = ${data.clazz_category}`
        let category = await database.executeQuery(categoryQuery)
        if (category.length > 0) { category = category[0] }

        query = `insert into ${Tables.dbCategory} (name, belong_to, icon, color_icon) values ("${data.clazz_sub_category_name}", ${data.clazz_category}, '${category.icon}', '${category.color_icon}')`

        let newCategory = await database.executeQuery(query)
        data.clazz_sub_category = newCategory.insertId
    }

    data.clazz_demo_images = JSON.stringify(data.clazz_demo_images)
    data.clazz_rooms = JSON.stringify(data.clazz_rooms)
    data.clazz_description = data.clazz_description.replace(/"/g, '\\"')
    data.clazz_description = data.clazz_description.replace(/'/g, "\\'")
    
    let today = new Date().yyyyMMddhhmm()

    query = `insert into ${Tables.dbClazzes} `
    query += `(clazz_title, clazz_price, currency, level, clazz_duration, clazz_category, clazz_sub_category, clazz_type, clazz_benefits, clazz_description, clazz_demo_images, clazz_images_description, clazz_requirement, clazz_note, clazz_rule, master_id, number_of_genius, min_session, clazz_rooms, teach_area, created_at, updated_at) `
    query += `values ("${data.clazz_title}", ${data.clazz_price}, "${data.currency}", "${data.level}",${data.clazz_duration}, ${data.clazz_category}, ${data.clazz_sub_category}, "${data.clazz_type}", "${data.clazz_benefits}", '${data.clazz_description}', '${data.clazz_demo_images}', '${data.clazz_images_description}', 
    '${data.clazz_requirement}', '${data.clazz_note}', '${data.clazz_rule}', ${data.master_id}, ${data.number_of_genius}, ${data.min_session}, ${data.clazz_rooms}, "${data.teach_area}", "${today}", "${today}")`

    let result = await database.executeQuery(query)
    return result 
}




async function fetchSubcategoryInCategory(categoryId) {
    let query = `select * from ${Tables.dbCategory} where belong_to = ${categoryId}` 
    let result = await database.executeQuery(query)
    return result
}


async function fetchLiveClazzes(masterId) {
    let query = `select * from ${Tables.dbClazzes}, ${Tables.dbCategory} where master_id = ${masterId} and clazz_category = id order by clazz_id desc`
    
    let results = await database.executeQuery(query)
    results.forEach(function(element) {
        let photos = element.clazz_demo_images
        photos = JSON.parse(photos)
        element.clazz_demo_images = photos

        element.category_icon = element.icon
        element.category_id = element.id
        element.category_name = element.name
        delete element.icon
        delete element.id
        delete element.name
    }, this);

    
    return results
}