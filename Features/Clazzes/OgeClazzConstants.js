module.exports.status = {
    waiting_for_verification: 'waiting_for_verification', 
    waiting_for_approval: 'waiting_for_approval',
    pending: 'pending',
    rejected: 'rejected',
    genius_cancel: 'genius_cancel',
    master_cancel: 'master_cancel',
    approved: 'approved',
    upcoming: 'upcoming', 
    in_progress: 'in_progress', 
    completed: 'completed'
}

module.exports.master_status = {
    approved: 'approved', 
    rejected: 'rejected', 
    pending: 'pending'
}

module.exports.verification_status = {
    not_submitted: 'not_submitted', 
    approved: 'approved', 
    rejected: 'rejected', 
    pending: 'pending'
}

module.exports.level = {
    beginner: 'Beginner', 
    intermediate: 'Intermediate', 
    advanced: 'Advanced', 
    all: 'All'
}

module.exports.error_status = {
    solved: 'solved', 
    pending: 'pending' 
}