let AuthCenter      = require('../Authentication/OgeAuthorizationCenter')
let Helper			= require('../../Supporter/OgePaginationHelpers')
let clazzConstant   = require('./OgeClazzConstants')
let clazzDatabase   = require('./OgeClazzDatabase')
let responser       = require('../../Supporter/OgeResponse')
let push			= require('../Notification/PushNotification')
let masterDatabase = require('../Masters/OgeMasterDatabase')

//___________________________________________________ EXPORTS ________________________________________

module.exports.fetchClazzDetail = fetchClazzDetail
module.exports.discoverClazzes = discoverClazzes
module.exports.fetchClazzReceipt = fetchClazzReceipt
module.exports.fetchClazzesInCategory = fetchClazzesInCategory
module.exports.searchClazz = searchClazz
module.exports.fetchClazzFaq = fetchClazzFaq
module.exports.fetchTopViewClazzes = fetchTopViewClazzes
module.exports.fetchClazzes = fetchClazzes
module.exports.createClazz = createClazz
module.exports.addClazzRoom = addClazzRoom
module.exports.fetchClazzRooms = fetchClazzRooms
module.exports.fetchSubcategory = fetchSubcategory
module.exports.fetchLiveClazzes = fetchLiveClazzes

async function fetchClazzes(req, res) {
    let clazzes = await clazzDatabase.fetchClazzes()

    if(clazzes) {
        clazzes = Formatter.removeUndefinedFieldsIn(clazzes)
        let response = Responser.response(200, clazzes)
        res.send(response)
    }
}


async function discoverClazzes(req, res) {

    let tracker = Tracker
    tracker.start()
    tracker.log(0, 'No track', 'discoverClazzes')
    
    let clazzes = await clazzDatabase.discoverClazzes()

    if(clazzes) {
        clazzes.forEach(function(element) {
            element = Formatter.removeUndefinedFieldsIn(element)
        }, this);

        let response = Responser.response(200, clazzes)
        res.send(response)
        tracker.end(response, 'success')
    }
}


async function fetchClazzDetail(req, res) {

    let tracker = Tracker
    tracker.start()

    tracker.log(0, 'No track', 'fetchClazzDetail')
    let clazzId = parseInt(req.params.id)
    let clazz = await clazzDatabase.fetchClazzDetail(clazzId)

    if(clazz) {
        clazz = Formatter.removeUndefinedFieldsIn(clazz)
        let response = Responser.response(200, clazz)
        res.send(response)
        clazzDatabase.updateViewCountOfClazz(clazzId)
        tracker.end(response, 'success')
    }
    else {
        let response = Responser.response(404, Responser.notExist)
        res.send(response)
        tracker.end(response, 'fail')
    }
}


async function fetchClazzReceipt(req, res) {
    let tracker = Tracker
    tracker.start()
    let genius = await AuthCenter.authorize(req)
    let isValidGenius = genius != null

    if (isValidGenius == false) {    
        let response = Responser.response(401, Responser.loginNeeded)
        res.send(response)
        
        tracker.log(null, 'Login needed', 'fetchClazzReceipt')
        tracker.end(response, 'fail')
        return
    }
    
    tracker.log(genius.genius_id, genius.full_name, 'fetchClazzReceipt')
    let bookId = parseInt(req.params.id)
    try {
        let clazz = await clazzDatabase.fetchClazzReceipt(bookId)
        
            if(clazz) {
                clazz = Formatter.removeUndefinedFieldsIn(clazz)
                let response = Responser.response(200, clazz)
                res.send(response)
                tracker.end(response, 'success')
            }
            else {
                let response = Responser.response(404, Responser.notExist)
                res.send(response)
                tracker.end(response, 'fail')
            }
    } catch (error) {
        let response = Responser.response(500, Responser.internalError)
        res.send(response)
        tracker.end(response, 'fail')
    }
    
}


async function fetchClazzesInCategory(req, res) {

    let tracker = Tracker
    tracker.start()
    tracker.log(0, 'No track', 'fetchClazzesInCategory')

    let categoryId = parseInt(req.params.id)
    let clazz = await clazzDatabase.fetchClazzesInCategory(categoryId)

    if(clazz) {
        clazz = Formatter.removeUndefinedFieldsIn(clazz)
        let response = Responser.response(200, clazz)
        res.send(response)
        tracker.end(response, 'success')
    }
    else {
        let response = Responser.response(200, [])
        res.send(response)
        tracker.end(response, 'fail')
    }
}


async function searchClazz(req, res) {
    let queryString = req.params.query
    queryString = queryString == null ? '' : queryString

    let foundClazzes = await clazzDatabase.getClazzesFromSearchQueries(queryString)
    if (foundClazzes == null) {
        res.send(Responser.response(200, []))
        return
    }

    (foundClazzes.length > 0) ? 
        res.send(Responser.response(200, foundClazzes)) : 
        res.send(Responser.response(200, []))
}


async function fetchClazzFaq(req, res) {

    let tracker = Tracker
    tracker.start()

    let clazzId = parseInt(req.params.id)
    let faqs = await clazzDatabase.fetchclazzFaq(clazzId)
    let response = Responser.response(200, faqs)
    res.send(response)

    tracker.log(0, 'No track', 'fetchclazzFaq')
    tracker.end(response, 'success')
}


async function fetchTopViewClazzes(req, res) {
    let page = (req.params.page) ? ((req.params.page > 1) ? req.params.page : 1) : 1
    let itemRange = Helper.generateNumberRecordsPagination(page)
    
    let clazzes = await clazzDatabase.fetchTopViewClazzes(itemRange)
    let response = Responser.response(200, clazzes, page)
    res.send(response)
}


async function createClazz(req, res) {
    if (authorizeMaster == false) { return }    

    let data = req.body
    let result = await clazzDatabase.createClazz(data)
    push.pushNewClazzToAdmin()
    res.send(responser.response(201, { message: `Your class is created and under review. It'll be live soon.` }))
}

async function addClazzRoom(req, res) {
    if (authorizeMaster == false) { return }

    let data = req.body
    let result = await clazzDatabase.addClazzRoom(data)
    res.send(responser.response(201, { room_id: result.insertId }))
}

async function fetchClazzRooms(req, res) {
    let master = await AuthCenter.authorize(req)
    let isValidMaster = master != null
    if (isValidMaster == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return false
    }

    let result = await clazzDatabase.fetchClazzRooms(master.genius_id)
    res.send(responser.response(200, result))
    
}

async function authorizeMaster(req, res) {
    let master = await AuthCenter.authorize(req)
    let isValidMaster = master != null
    if (isValidMaster == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return false
    }

    let isMaster = await masterDatabase.checkIsMaster(master.genius_id)
    if (isMaster == false) {
        res.send(Responser.response(550, Responser.noPermission))
        return false
    }

    return true
}

async function fetchSubcategory(req, res) {
    let category_id = req.params.id
    let results = await clazzDatabase.fetchSubcategoryInCategory(category_id)
    res.send(Responser.response(200, results))
}

async function fetchLiveClazzes(req, res) {
    let master = await AuthCenter.authorize(req)
    let isValidMaster = master != null
    if (isValidMaster == false) {
        res.send(Responser.response(401, Responser.loginNeeded))
        return false
    }

    let result = await clazzDatabase.fetchLiveClazzes(master.genius_id)
    res.send(Responser.response(200, result))
}