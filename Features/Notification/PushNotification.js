let FCM = require('fcm-push')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let clazzConstant         = require('../Clazzes/OgeClazzConstants')

let serverKey = 'AAAARsil1nY:APA91bH7zs9n9hT-Ts0vXCwH44zQsDos8flcegkeCB5FruVCt_Gjn2hsuaB0A276UQQQRaw6zC-y9Ggny--INZ7fBDzDKdnEUngOMP5Q-sv8qPfEM7XXAYuUpti4CKwi3xskK_nxQX8b'
let fcm = new FCM(serverKey)

module.exports.pushApprovedMessage = pushApprovedMessage
module.exports.pushRejectedMessage = pushRejectedMessage
module.exports.pushBookedMessage = pushBookedMessage
module.exports.pushGeniusCancelMessage = pushGeniusCancelMessage
module.exports.pushMasterCancelMessage = pushMasterCancelMessage
module.exports.pushNewClazzToAdmin = pushNewClazzToAdmin
module.exports.pushNewMasterToAdmin = pushNewMasterToAdmin
module.exports.pushNewVerificationToAdmin = pushNewVerificationToAdmin
module.exports.pushAdminApproveClazz = pushAdminApproveClazz
module.exports.pushAdminRejectClazz = pushAdminRejectClazz
module.exports.pushAdminApproveVerification = pushAdminApproveVerification
module.exports.pushAdminRejectVerification = pushAdminRejectVerification
module.exports.pushAdminRejectMaster = pushAdminRejectMaster
module.exports.pushAdminApproveMaster = pushAdminApproveMaster
module.exports.pushReportToAdmin = pushReportToAdmin
module.exports.pushNeedHelpToAdmin = pushNeedHelpToAdmin


let pushTypes = {
    "master_clazz_booked": "master_clazz_booked", 
    "genius_booking_approved": "genius_booking_approved", 
    "genius_booking_rejected": "genius_booking_rejected",
    "master_approved_booking": "master_approved_booking",
    "master_rejected_booking": "master_rejected_booking",
    "genius_cancel_clazz": "genius_cancel_clazz",
    "master_cancel_clazz": "master_cancel_clazz",
    "new_clazz_to_admin": "new_clazz_to_admin",
    "admin_approve_clazz": "admin_approve_clazz",
    "admin_reject_clazz": "admin_reject_clazz", 
    "admin_reject_verification": "admin_reject_verification",
    "admin_approve_verification": "admin_approve_verification",
    "admin_approve_master": "admin_approve_master",
    "admin_reject_master": "admin_reject_master",
    "new_master_to_admin": "new_master_to_admin",
    "new_verification_to_admin": "new_verification_to_admin",
    "new_clazz_to_admin": "new_clazz_to_admin",
    "new_error_to_admin": "new_error_to_admin",
    "need_help_to_admin": "need_help_to_admin"
}

module.exports.pushTypes = pushTypes


async function getTokenFrom(geniusId) {
    let query = 'select device_token from ' + Tables.dbGeniuses  + ' '
    query += 'where genius_id = ' + geniusId
    let token = await database.executeQuery(query)
    if (token == null || token.length == 0) { return null }
    return token[0].device_token
}


// Master approve booking 

async function pushApprovedMessage(data) {

    let tokens = await getTokenFrom(data.genius_id) 

    data.tokens = tokens
    data.content = 'Master ' + data.master_name + ' has approved your booking for class ' + data.clazz_title
    insertResult = await insertApprovedNotificationToDatabase(data)
    data.notification_id = insertResult.insertId
    let result = await sendApprovedMessage(data)
}

async function insertApprovedNotificationToDatabase(data) {
    let now = new Date().yyyyMMddhhmm()
    query = 'insert into ' + Tables.dbNotification + ' '
    query += '(push_type, booking_id, from_genius_id, to_genius_id, content, clazz_id, clazz_title, created_at) '
    query += `values ("${pushTypes.genius_booking_approved}", ${data.booking_id}, ${data.master_id}, ${data.genius_id}, "${data.content}", ${data.clazz_id}, "${data.clazz_title}", "${now}")`

    insertResult = await database.executeQuery(query)
    return insertResult
}

async function sendApprovedMessage(data) {
    let message = {
        to: data.tokens, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.genius_booking_approved,
            notification_id: data.notification_id,
            clazz_id: data.clazz_id,
            clazz_title: data.clazz_title,
            master_id: data.master_id, 
            master_name: data.master_name, 
            master_profile_image: data.master_profile_image,
            content: data.content, 
        },
        notification: {
            title: 'Booking approved',
            body: data.content
        }
    }

    let result = await fcm.send(message)
}



// Master reject booking

async function pushRejectedMessage(data) {

    let tokens = await getTokenFrom(data.genius_id) 

    data.tokens = tokens
    data.content = `Master ${data.master_name} has rejected your booking for class ${data.clazz_title}. Reason: ${data.rejected_reason}`
    insertResult = await insertRejectedNotificationToDatabase(data)
    data.notification_id = insertResult.insertId
    let result = await sendRejectedMessage(data)
}

async function insertRejectedNotificationToDatabase(data) {
    let now = new Date().yyyyMMddhhmm()
    query = 'insert into ' + Tables.dbNotification + ' '
    query += '(push_type, booking_id, from_genius_id, to_genius_id, content, clazz_id, clazz_title, rejected_reason, created_at) '
    query += `values ("${pushTypes.genius_booking_rejected}", ${data.booking_id}, ${data.master_id}, ${data.genius_id}, "${data.content}", ${data.clazz_id}, "${data.clazz_title}", "${data.rejected_reason}", "${now}")`

    insertResult = await database.executeQuery(query)
    return insertResult
}

async function sendRejectedMessage(data) {
    let message = {
        to: data.tokens, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.genius_booking_approved,
            notification_id: data.notification_id,
            clazz_id: data.clazz_id,
            clazz_title: data.clazz_title,
            master_id: data.master_id, 
            master_name: data.master_name, 
            master_profile_image: data.master_profile_image,
            content: data.content,
            rejected_reason: data.rejected_reason
        },
        notification: {
            title: 'Booking rejected',
            body: data.content
        }
    }

    let result = await fcm.send(message)
}





// Genius book clazz

async function pushBookedMessage(data) {
    // data = master_id, genius_name, clazz_title, clazz_id, booking_id
    let tokens = await getTokenFrom(data.master_id) 
    data.tokens = tokens    
    data.content = 'Genius ' + data.genius_name + ' has just booked for class ' + data.clazz_title
    insertResult = await insertBookedNotificationToDatabase(data)
    data.notification_id = insertResult.insertId
    await sendBookedMessage(data)
}

async function insertBookedNotificationToDatabase(data) {
    let now = new Date().yyyyMMddhhmm()
    let query = `insert into ${Tables.dbNotification} (push_type, booking_id, from_genius_id, to_genius_id, content, clazz_id, clazz_title, created_at) values ('${pushTypes.master_clazz_booked}', ${data.booking_id}, ${data.genius_id}, ${data.master_id}, '${data.content}', ${data.clazz_id}, '${data.clazz_title}', '${now}') `
    let insertResult = await database.executeQuery(query)
    return insertResult
}

async function sendBookedMessage(data) {

    let message = {
        to: data.tokens, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.master_clazz_booked,
            notification_id: data.notification_id,
            clazz_id: data.clazz_id,
            clazz_title: data.clazz_title,
            genius_id: data.master_id, 
            genius_name: data.master_name, 
            genius_profile_image: data.master_profile_image,
            content: data.content,
            schedules: data.schedules
        },
        notification: {
            title: 'Booking received',
            body: data.content
        }
    }

    await fcm.send(message)
}



// Cancel clazz

async function pushMasterCancelMessage(data) {
    let tokens = await getTokenFrom(data.master_id) 
    data.push_type = pushTypes.master_cancel_clazz
    data.to_genius_id = data.genius_id
    data.from_genius_id = data.master_id
    data.tokens = tokens    
    insertResult = await insertMasterCancelNotificationToDatabase(data)
    data.notification_id = insertResult.insertId
    await sendMasterCancelMessage(data)
}

async function insertMasterCancelNotificationToDatabase(data) {
    let now = new Date().yyyyMMddhhmm()
    let query = `insert into ${Tables.dbNotification} (push_type, booking_id, from_genius_id, to_genius_id, content, clazz_id, clazz_title, created_at) values ('${data.push_type}', ${data.booking_id}, ${data.from_genius_id}, ${data.to_genius_id}, '${data.content}', ${data.clazz_id}, '${data.clazz_title}', '${now}') `
    let insertResult = await database.executeQuery(query)
    return insertResult
}

async function sendMasterCancelMessage(data) {
    let message = {
        to: data.tokens, 
        collapse_key: 'none', 
        data: {
            push_type: data.push_type,
            notification_id: data.notification_id,
            clazz_id: data.clazz_id,
            clazz_title: data.clazz_title,
            genius_id: data.master_id, 
            genius_name: data.master_name, 
            genius_profile_image: data.genius_profile_image,
            content: data.content,
            schedules: data.schedules
        },
        notification: {
            title: 'Master cancelled class',
            body: data.content
        }
    }

    await fcm.send(message)
}


async function pushGeniusCancelMessage(data) {
    let tokens = await getTokenFrom(data.master_id) 
    data.push_type = pushTypes.genius_cancel_clazz
    data.to_genius_id = data.master_id
    data.from_genius_id = data.genius_id
    data.tokens = tokens    
    insertResult = await insertGeniusCancelNotificationToDatabase(data)
    data.notification_id = insertResult.insertId
    await sendGeniusCancelMessage(data)
}

async function insertGeniusCancelNotificationToDatabase(data) {
    let now = new Date().yyyyMMddhhmm()
    let query = `insert into ${Tables.dbNotification} (push_type, booking_id, from_genius_id, to_genius_id, content, clazz_id, clazz_title, created_at) values ('${data.push_type}', ${data.booking_id}, ${data.genius_id}, ${data.master_id}, '${data.content}', ${data.clazz_id}, '${data.clazz_title}', '${now}') `
    let insertResult = await database.executeQuery(query)
    return insertResult
}

async function sendGeniusCancelMessage(data) {
    let message = {
        to: data.tokens, 
        collapse_key: 'none', 
        data: {
            push_type: data.push_type,
            notification_id: data.notification_id,
            clazz_id: data.clazz_id,
            clazz_title: data.clazz_title,
            genius_id: data.genius_id, 
            genius_name: data.genius_name, 
            genius_profile_image: data.genius_profile_image,
            content: data.content,
            schedules: data.schedules
        },
        notification: {
            title: 'Genius cancelled class',
            body: data.content
        }
    }

    await fcm.send(message)
}


// Admin zone

async function pushNewClazzToAdmin() {
    let admins = await fetchAdminDetail()
    admins.forEach(async function(admin) {
        let content = "New class is waiting for approval"
        let token = admin.device_token
        let admin_id = admin.genius_id
        let message = {
            to: token, 
            collapse_key: 'none', 
            data: {
                push_type: pushTypes.new_clazz_to_admin
            },
            notification: {
                title: 'Admin - Class pending',
                body: "New class is waiting for approval"
            }
        }
    
        await fcm.send(message)
        insertAdminNotification(pushTypes.new_clazz_to_admin, admin_id, content)
        
    }, this);
}

async function fetchAdminDetail() {
    let query = `select device_token, genius_id from ${Tables.dbGeniuses} where genius_id in (select genius_id from ${Tables.dbAdmin})`
    let tokens = await database.executeQuery(query)
    return tokens
}

async function pushNewMasterToAdmin() {
    let admins = await fetchAdminDetail()
    admins.forEach(async function(admin) {
        let content = "New Master is waiting for approval"
        let token = admin.device_token
        let admin_id = admin.genius_id
        let message = {
            to: token, 
            collapse_key: 'none', 
            data: {
                push_type: pushTypes.new_clazz_to_admin
            },
            notification: {
                title: 'Admin - New Master application',
                body: content
            }
        }
    
        await fcm.send(message)
        insertAdminNotification(pushTypes.new_master_to_admin, admin_id, content)
    }, this);
}

async function pushReportToAdmin() {
    let admins = await fetchAdminDetail()
    admins.forEach(async function(admin) {
        let content = "New error detected. Please tell Ky asap"
        let push_type = pushTypes.new_error_to_admin
        let token = admin.device_token
        let admin_id = admin.genius_id
        let message = {
            to: token, 
            collapse_key: 'none', 
            data: {
                push_type: push_type
            },
            notification: {
                title: 'Admin - New error',
                body: content
            }
        }
    
        await fcm.send(message)
        insertAdminNotification(push_type, admin_id, content)
    }, this);
}

async function pushNeedHelpToAdmin(genius_id, user_message) {
    let admins = await fetchAdminDetail()
    admins.forEach(async function(admin) {
        let content = `Genius needs help. Content: ${user_message}`
        let push_type = pushTypes.need_help_to_admin
        let token = admin.device_token
        let admin_id = admin.genius_id
        let message = {
            to: token, 
            collapse_key: 'none', 
            data: {
                push_type: push_type
            },
            notification: {
                title: 'Admin - Genius needs help',
                body: content
            }
        }
    
        await fcm.send(message)
        insertAdminNotificationFromGenius(push_type, genius_id, admin_id, content)
    }, this);
}

async function insertAdminNotificationFromGenius(type, from_genius_id, admin_id, content) {
    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, created_at) values ('${type}', ${from_genius_id}, ${admin_id}, '${content}', '${now}') `
    database.executeQuery(query)
}

async function insertAdminNotification(type, admin_id, content) {
    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, created_at) values ('${type}', 0, ${admin_id}, '${content}', '${now}') `
    database.executeQuery(query)
}

async function pushNewVerificationToAdmin() {
    let admins = await fetchAdminDetail()
    admins.forEach(async function(admin) {
        let content = "New Genius is waiting for verification"
        let token = admin.device_token
        let admin_id = admin.genius_id
        let message = {
            to: token, 
            collapse_key: 'none', 
            data: {
                push_type: pushTypes.new_clazz_to_admin
            },
            notification: {
                title: 'Admin - New Genius verification',
                body: content
            }
        }
    
        await fcm.send(message)
        insertAdminNotification(pushTypes.new_verification_to_admin, admin_id, content)
        
    }, this);
}

async function pushAdminRejectClazz(clazz_id, admin_id, reason) {
    let query = `select device_token, genius_id master_id, clazz_title from ${Tables.dbGeniuses}, ${Tables.dbClazzes} where genius_id = master_id and clazz_id = ${clazz_id}`
    let data = await database.executeQuery(query)
    data = data[0]

    let content = `Your class ${data.clazz_title} is rejected. Reason: ${reason}`
    let message = {
        to: data.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_reject_clazz,
            clazz_id: clazz_id,
            clazz_title: data.clazz_title,
            genius_id: data.genius_id, 
            content: content
        },
        notification: {
            title: 'Class rejected',
            body: content
        }
    }
    await fcm.send(message)
    
    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, rejected_reason, clazz_id, clazz_title, created_at) values ('${pushTypes.admin_reject_clazz}', ${admin_id}, ${data.master_id}, '${content}', "${reason}", ${clazz_id}, '${data.clazz_title}', '${now}') `
    let insertResult = await database.executeQuery(query)
}

async function pushAdminApproveClazz(clazz_id, admin_id)  {
    let query = `select device_token, genius_id master_id, clazz_title from ${Tables.dbGeniuses}, ${Tables.dbClazzes} where genius_id = master_id and clazz_id = ${clazz_id}`
    let data = await database.executeQuery(query)
    data = data[0]

    let content = `Your class ${data.clazz_title} is approved and live. Geniuses will book your class soon.`
    let message = {
        to: data.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_approve_clazz,
            clazz_id: clazz_id,
            clazz_title: data.clazz_title,
            genius_id: data.genius_id, 
            content: content
        },
        notification: {
            title: 'New class approved',
            body: content
        }
    }

    await fcm.send(message)

    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, clazz_id, clazz_title, created_at) values ('${pushTypes.admin_approve_clazz}', ${admin_id}, ${data.master_id}, '${content}', ${clazz_id}, '${data.clazz_title}', '${now}') `
    let insertResult = await database.executeQuery(query)
}

async function pushAdminApproveVerification(genius_id, admin_id)  {
    let query = `select device_token from ${Tables.dbGeniuses} where genius_id = ${genius_id}`
    let token = await database.executeQuery(query)
    token = token[0]

    let content = `Your verification is completed. Start unlimited learning now.`
    let message = {
        to: token.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_approve_verification,
            content: content
        },
        notification: {
            title: 'Verification approved',
            body: content
        }
    }

    await fcm.send(message)

    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, created_at) values ('${pushTypes.admin_approve_verification}', ${admin_id},${genius_id}, '${content}', '${now}') `
    database.executeQuery(query)


}

async function pushAdminRejectVerification(genius_id, admin_id, reason) {
    let query = `select device_token from ${Tables.dbGeniuses} where genius_id = ${genius_id}`
    let token = await database.executeQuery(query)
    token = token[0]

    let content = `Your verification is rejected. Reason: ${reason}. Update new document and submit again. `
    let message = {
        to: token.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_reject_verification,
            content: content
        },
        notification: {
            title: 'Verification Rejected',
            body: content
        }
    }
    await fcm.send(message)
    
    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, rejected_reason, created_at) values ('${pushTypes.admin_reject_verification}', ${admin_id}, ${genius_id}, '${content}', "${reason}", '${now}') `
    let insertResult = await database.executeQuery(query)
}

async function pushAdminApproveMaster(master_id, admin_id)  {
    let query = `select device_token from ${Tables.dbGeniuses} where genius_id = ${master_id}`
    let token = await database.executeQuery(query)
    token = token[0]

    let content = `Your application to become Master is approved. Welcome to Ogenii, Master.`
    let message = {
        to: token.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_approve_master,
            content: content
        },
        notification: {
            title: 'Application approved',
            body: content
        }
    }

    await fcm.send(message)

    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, created_at) values ('${pushTypes.admin_approve_master}', ${admin_id}, ${master_id}, '${content}', '${now}') `
    database.executeQuery(query)


}

async function pushAdminRejectMaster(master_id, admin_id, reason) {
    let query = `select device_token from ${Tables.dbGeniuses} where genius_id = ${master_id}`
    let token = await database.executeQuery(query)
    token = token[0]

    let content = `Your application to become master is rejected. Reason: ${reason}. Update new document and submit again. `
    let message = {
        to: token.device_token, 
        collapse_key: 'none', 
        data: {
            push_type: pushTypes.admin_reject_master,
            content: content
        },
        notification: {
            title: 'Verification Rejected',
            body: content
        }
    }
    await fcm.send(message)
    
    let now = new Date().yyyyMMddhhmm()
    query = `insert into ${Tables.dbNotification} (push_type, from_genius_id, to_genius_id, content, rejected_reason, created_at) values ('${pushTypes.admin_reject_master}', ${admin_id}, ${master_id}, '${content}', "${reason}", '${now}') `
    let insertResult = await database.executeQuery(query)
}