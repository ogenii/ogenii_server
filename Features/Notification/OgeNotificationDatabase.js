const Promise               = require('promise')
let Formatter           = require('../../Supporter/OgeFormatter')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let Push                    = require('../Notification/PushNotification')

module.exports.markNotificationRead = markNotificationRead
module.exports.markAllNotiRead = markAllNotiRead
module.exports.fetchGeniusNotifcations = fetchGeniusNotifcations



async function fetchGeniusNotifcations(geniusId) {

    let query = `select notification.*, genius.full_name from_genius_name, genius.profile_image from_genius_profile_image from ${Tables.dbNotification} notification, ${Tables.dbGeniuses} genius where to_genius_id = ${geniusId} and notification.from_genius_id = genius.genius_id order by created_at desc`
    let notifications = await database.executeQuery(query)
    notifications.forEach(function(item) {
        item = Formatter.removeUndefinedFieldsIn(item)
    }, this);



    return notifications
}

async function markNotificationRead(geniusId, notiId) {
    let query = 'UPDATE ' + Tables.dbNotification + ' '
    query += 'SET is_read = true WHERE to_genius_id=' + geniusId + ' AND id=' + notiId

    let result = await database.executeQuery(query)
    return result.changedRows > 0
}

function markAllNotiRead(geniusId) {
    // let query = 'UPDATE ' + Tables.dbNotification + 'SET is_read=true WHERE to_genius_id=' + geniusId
    // let result = await database.executeQuery(query)
    // return result
}