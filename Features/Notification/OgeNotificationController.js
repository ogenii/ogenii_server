let PaginationHelper      = require('../../Supporter/OgePaginationHelpers')
let AuthCenter            = require('../Authentication/OgeAuthorizationCenter')
let notificationDatabase  = require('./OgeNotificationDatabase.js')

//___________________________________________________ EXPORTS ________________________________________
module.exports.fetchGeniusNotifcations = fetchGeniusNotifcations
module.exports.markSpecificNotiRead = markSpecificNotiRead
module.exports.markAllNotiRead = markAllNotiRead







async function fetchGeniusNotifcations(req, res) {

    let tracker = Tracker
    tracker.start()
    let genius = await AuthCenter.authorize(req)
    let isValid = genius != null 

    if (isValid) {
        tracker.log(genius.genius_id, genius.full_name, 'fetchGeniusNotifcations')
        let notifications = await notificationDatabase.fetchGeniusNotifcations(genius.genius_id)
        let response = Responser.response(200, notifications)
        res.send(response)
        tracker.end(response, 'success')
    }
    else {
        tracker.log(null, 'Login needed', 'fetchGeniusNotifcations')
        let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
        tracker.end(response, 'fail')
    }
}

async function markSpecificNotiRead(req, res) {

    let tracker = Tracker
    tracker.start()
    let genius = await AuthCenter.authorize(req)
    let isValid = genius != null 

    if (isValid) {
        tracker.log(genius.genius_id, genius.full_name, 'markSpecificNotiRead')
        let isSuccess = await notificationDatabase.markNotificationRead(genius.genius_id, parseInt(req.params.notiId))
        let response = Responser.response(200, { is_success: isSuccess })
        res.send(response)
        tracker.end(response, 'success')
    }
    else {
        tracker.log(null, 'Login needed', 'markSpecificNotiRead')
        let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
        tracker.end(response, 'fail')
    }
}

function markAllNotiRead(req, res) {
    AuthCenter.authorize(req).then(genius => {
        return notificationDatabase.markAllNotiRead(parseInt(genius.genius_id))
    }).then(result => {
        let response = Responser.response(200, Responser.done)
        res.send(response)
    }).catch(
        code => {
            log('request fail with err', code)

            let message
            switch (code) {
                case 404:
                    message = Responser.notExist
                    break;

                default:
                    message = Responser.internalError
                    break;
            }

            let response = Responser.response(code, { message: message})
            res.send(response)
    })
}


