let AuthCenter          = require('../Authentication/OgeAuthorizationCenter')
let push                = require('../Notification/PushNotification')
let geniusDatabase      = require('./OgeGeniusDatabase')
let Tables              = require('../../DatabaseConnector/OgeDatabaseTables')

//___________________________________________________ EXPORTS 
module.exports.fetchGeniusDetail = fetchGeniusDetail
module.exports.bookClazz = bookClazz
module.exports.registerDevice = registerDevice
module.exports.updateGeniusDetail = updateGeniusDetail
module.exports.verifyGenius = verifyGenius
module.exports.fetchLearningProgress = fetchLearningProgress
module.exports.fetchSchedules = fetchSchedules
module.exports.checkVerificationSubmitted = checkVerificationSubmitted





// ___________________________________________________ VERIFY GENIUS

async function verifyGenius(req, res) {

    let requestedGenius = await AuthCenter.authorize(req)
    let isValidGenius = requestedGenius != null

    if (isValidGenius == false) { 
        res.send(Responser.response(401, { message: Responser.loginNeeded }))
        return 
    }

    let request = req.body
    request.genius_id = requestedGenius.genius_id

    let data = saveVerificationDataFromRaw(request)
    let result = await geniusDatabase.verifyGenius(data)
    if (result.status == 200) {
        res.send(Responser.response(200, { message: 'Your verification will be processed in 2-4 hours' }))
        push.pushNewVerificationToAdmin()
    }
    else {
        res.send(Responser.response(result.status, { message: result.message }))
    }

    let updateData = { full_name: request.full_name, birthdate: request.birthdate, email: request.email, phone_number: request.phone }
    geniusDatabase.updateGeniusDetail(requestedGenius.genius_id, updateData)
}

function saveVerificationDataFromRaw(request) {
    let data = {} 
    data.genius_id = request.genius_id
    data.full_name = request.full_name
    data.birthdate = request.birthdate
    data.document_type = request.document_type
    data.document_photo = request.document_photo
    data.selfie_photo = request.selfie_photo
    data.profile_image = request.profile_image
    data.phone = request.phone
    data.email = request.email
    return data
}





// ___________________________________________________ UPDATE GENIUS DETAIL

async function updateGeniusDetail(req, res) {
    let geniusId = req.params.id
    let geniusDetail = req.body

    let updateResult = await geniusDatabase.updateGeniusDetail(geniusId, geniusDetail)
    if (updateResult) {
        res.send(Responser.response(200, {message: Responser.done}))
    }
    else {
        res.send(Responser.response(500, {message: Responser.internalError}))
    }
}





//___________________________________________________ REQUEST clazz 
async function bookClazz(req, res) {

    let tracker = Tracker
    tracker.start()
    let requestedGenius = await AuthCenter.authorize(req)
    let isValid = requestedGenius != null 

    if (isValid) {
        tracker.log(requestedGenius.genius_id, requestedGenius.full_name, 'bookClazz')
        let data = req.body
        data.genius_id = requestedGenius.genius_id
        data.did_pay = false
        
        let bookedResult = await geniusDatabase.bookClazz(data)
        let response = Responser.response(bookedResult.code, { message: bookedResult.message })
        res.send(response)
        tracker.end(response, 'success')

        if (bookedResult.code == 2011) { return }

        let query = 'select device_token token, master_id, clazz_id clazz_id, clazz_title clazz_title '
        query += 'from ' + Tables.dbGeniuses + ', ' + Tables.dbClazzes + ' clazz '
        query += 'where genius_id = clazz.master_id' + ' and clazz_id = ' + parseInt(req.body.clazz_id)

        let pushData = await database.executeQuery(query)
        pushData = pushData != null && pushData.length > 0 ? pushData[0] : null
        if (pushData == null) { return }

        pushData.booking_id = bookedResult.booking_id
        pushData.genius_id = requestedGenius.genius_id
        pushData.genius_name = requestedGenius.full_name
        pushData.genius_profile_image = requestedGenius.profile_image
        push.pushBookedMessage(pushData)

    }
    else {
        tracker.log(null, 'Login needed', 'bookClazz')
        let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
        tracker.end(response, 'fail')
    }
}



async function fetchGeniusDetail(req, res) {

    let geniusId = parseInt(req.params.id)
    let foundGenius = await geniusDatabase.fetchGeniusDetail(geniusId)
    if (foundGenius) {
        const response = Responser.response(200, foundGenius)
        res.send(response)
    }
    else {
        const response = Responser.response(404, Responser.notExist)
        res.send(response)
    }
}


async function registerDevice(req, res) {

    let tracker = Tracker
    tracker.start()
    let requestedGenius = await AuthCenter.authorize(req)
    let isValid = requestedGenius != null 

    if (isValid) {
        tracker.log(requestedGenius.genius_id, requestedGenius.full_name, 'registerDevice')
        let deviceToken = req.body.device_token
        let result = await geniusDatabase.registerDeviceToGenius(deviceToken, requestedGenius.genius_id)
        tracker.end(result, 'success')
    } 
    else {
        tracker.log(null, 'Login needed', 'registerDevice')
        let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
        tracker.end(response, 'fail')
    }
}



async function fetchLearningProgress(req, res) {

    let tracker = Tracker
    tracker.start()
    let requestedGenius = await AuthCenter.authorize(req)
    let isValid = requestedGenius != null 

    if (isValid) {
        let geniusId = requestedGenius.genius_id
        tracker.log(geniusId, requestedGenius.full_name, 'fetchLearningProgress')
        let learnings = await geniusDatabase.fetchLearning(geniusId)
        let response = Responser.response(200, learnings)
        res.send(response)
        tracker.end(response, 'success')
        return 
    }

    tracker.log(null, 'Login needed', 'fetchLearningProgress')
    let response = Responser.response(401, { message: Responser.loginNeeded })
    res.send(response)
    tracker.end(response, 'fail')
}




async function fetchSchedules(req, res) {

    let tracker = Tracker
    tracker.start()
    let requestedGenius = await AuthCenter.authorize(req)
    let isValid = requestedGenius != null 

    if (isValid) {
        let geniusId = requestedGenius.genius_id
        tracker.log(geniusId, requestedGenius.full_name, 'fetchSchedules')
        let schedules = await geniusDatabase.fetchSchedules(geniusId)
        let response = Responser.response(200, schedules)
        res.send(response)
        tracker.end(response, 'success')
        return 
    }
    
    tracker.log(null, 'Login needed', 'fetchSchedules')
    let response = Responser.response(401, { message: Responser.loginNeeded })
    res.send(response)
    tracker.end(response, 'fail')



}



async function checkVerificationSubmitted(req, res) {
    let requestedGenius = await AuthCenter.authorize(req)
    let isValid = requestedGenius != null 

    if (isValid) {
        let geniusId = requestedGenius.genius_id
        let submitted = await geniusDatabase.checkVerificationSubmitted(geniusId)
        let response = Responser.response(200, { "submitted": submitted })
        res.send(response)
        return 
    }

    let response = Responser.response(401, { message: Responser.loginNeeded })
    res.send(response)
}