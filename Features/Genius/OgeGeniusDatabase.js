let clazzConstant         = require('../Clazzes/OgeClazzConstants')
let DateExtension         = require('../../Supporter/DateExtension')
let Formatter             = require('../../Supporter/OgeFormatter')
let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let adminDatabase         = require('../Admin/OgeAdminDatabase')

module.exports.bookClazz = bookClazz
module.exports.fetchGeniusDetail = fetchGeniusDetail
module.exports.registerDeviceToGenius = registerDeviceToGenius
module.exports.updateGeniusDetail = updateGeniusDetail
module.exports.insertGeniusIntoDatabase = insertGeniusIntoDatabase
module.exports.updateGeniusTokenIntoDatabase = updateGeniusTokenIntoDatabase
module.exports.verifyGenius = verifyGenius
module.exports.fetchLearning = fetchLearning
module.exports.fetchSchedules = fetchSchedules
module.exports.fetchGeniusWithFacebookId = fetchGeniusWithFacebookId
module.exports.checkVerificationSubmitted = checkVerificationSubmitted





//___________________________________________________ REQUEST clazz 

async function doesVerificationExist(geniusId) { 

    let query = 'SELECT * FROM ' + Tables.dbGeniusVerification + ' WHERE genius_id=' + geniusId
    let verification = await database.executeQuery(query)

    if (verification.length > 0) {
        return true 
    }
    return false 
}

async function verifyGenius(data) {

    let today = (new Date()).yyyyMMddhhmm()
    log('verifyGenius', data)
    let didExist = await doesVerificationExist(data.genius_id)
    
    let query = ''
    let genius_id = data.genius_id
    if (didExist) {
        query = `update ${Tables.dbGeniusVerification} set full_name = "${data.full_name}", birthdate = "${data.birthdate}", document_type = "${data.document_type}", document_photo = "${data.document_photo}", selfie_photo = "${data.selfie_photo}", status = "${clazzConstant.verification_status.pending}", updated_at = "${today}" where genius_id = ${genius_id}`
    }
    else {
        query = `insert into ${Tables.dbGeniusVerification} (genius_id, full_name, birthdate, document_type, document_photo, selfie_photo, created_at) values (${data.genius_id}, "${data.full_name}", "${data.birthdate}", "${data.document_type}", "${data.document_photo}", "${data.selfie_photo}", "${today}")`  
    }

    let updateGeniusTable = ''
    if (data.profile_image != ' ' && data.profile_image != null) {
        updateGeniusTable = `update ${Tables.dbGeniuses} set full_name = '${data.full_name}', profile_image = '${data.profile_image}' where genius_id = ${genius_id}`
    }
    else {
        updateGeniusTable = `update ${Tables.dbGeniuses} set full_name = '${data.full_name}' where genius_id = ${genius_id}`
    }
    database.executeQuery(updateGeniusTable)

    try {
        let result = await database.executeQuery(query)
        return { 'status': 200, 'message': 'Success' }
    }
    catch (err) {
        return { 'status': 500, 'message': err }
    }
}







async function fetchGeniusDetail(genius_id) {
    try {
        const fetchStatement = 'SELECT * FROM ' + Tables.dbGeniuses + ' WHERE genius_id = ' + genius_id
        let result = await database.executeQuery(fetchStatement)

        let genius = {}
        if (result.length > 0) {
            genius = result[0]
            delete genius.login_service_token
            delete genius.login_service_id
            genius = Formatter.removeUndefinedFieldsIn(genius)
        }

        let isAdmin = await adminDatabase.checkIsAdmin(genius_id)
        genius.is_admin = isAdmin

        query = `select status verification_status from ${Tables.dbGeniusVerification} where genius_id = ${genius_id}`
        let status = await database.executeQuery(query)
        if (status.length > 0) { 
            status = status[0] 
            genius.verification_status = status.verification_status
        }

        query = `select status master_status from ${Tables.dbMasters} where master_id = ${genius_id}`
        status = await database.executeQuery(query)
        if (status.length > 0) { 
            status = status[0] 
            genius.master_status = status.master_status
        }
        
        return genius

    } catch (error) {
        console.log(error)
        return null
    }
}


async function insertGeniusIntoDatabase(genius) {
    let today = new Date().yyyyMMddhhmm()
    let query = 'INSERT INTO ' + Tables.dbGeniuses + ' '
    query += '(first_name, last_name, full_name, profile_image, email, login_service_id, login_service_token, service_provider, registered_at) '
    query += `VALUES ('${genius.first_name}', '${genius.last_name}', '${genius.full_name}', '${genius.profile_image}', '${genius.email}', '${genius.login_service_id}', '${genius.login_service_token}', '${genius.service_provider}', '${today}') `

    let result = await database.executeQuery(query)
    genius.genius_id = result.insertId
    genius.is_master = 0
    return genius
}


function updateGeniusTokenIntoDatabase(genius) {
    let query = 'UPDATE ' + Tables.dbGeniuses + ' SET ogenii_token = \"' + genius.ogenii_token + '\" WHERE genius_id = ' + genius.genius_id
    database.executeQuery(query)
}


async function bookClazz(data) {

    // data.schedules: array of { date_time, location_long, location_lat, address }
    let query = `select genius_id from ${Tables.dbGeniusVerification} where genius_id = ${data.genius_id} and status = 'approved'`
    let isVerified = await database.executeQuery(query)
    isVerified = isVerified.length > 0
    let status = isVerified == true ? clazzConstant.status.waiting_for_approval : clazzConstant.status.waiting_for_verification

    let today = (new Date()).yyyyMMddhhmm()
    let schedules = data.schedules

    if (data.book_mode == 'clazz') {
        let query = `select * from ${Tables.dbClazzRoom} clazz_room, ${Tables.dbClazzes} clazz where clazz.clazz_id = ${data.clazz_id} and clazz.clazz_rooms = clazz_room.room_id`
        let room = await database.executeQuery(query)
        if (room.length > 0) {
            room = room[0]
            data.location_lat = room.latitude
            data.location_long = room.longitude
            data.address = room.address
        }
        else {
            data.location_lat = 0 
            data.location_long = 0
            data.address = 'Undefined'
        }
    }

    query = `insert into ${Tables.dbLearn} (genius_id, clazz_id, book_mode, payment_method, did_pay, created_at) `
    query += `values (${data.genius_id}, ${data.clazz_id}, '${data.book_mode}', "${data.payment_method}", ${data.did_pay}, "${today}")`
    let insertToLearnResult = await database.executeQuery(query)
    let booking_id = insertToLearnResult.insertId

    query = ""
    for (var i = 0; i < data.schedules.length; i++) {
        let element = data.schedules[i];
        query += `insert into ${Tables.dbLearnSchedules} (booking_id, date_time, location_lat, location_long, address, status) values (${booking_id}, '${element.date_time}', ${element.location_lat}, ${element.location_long}, '${element.address}', '${status}');`
    }
    database.executeQuery(query)

    if (insertToLearnResult) {
        if (status == clazzConstant.waiting_for_verification) {
            return { 'code': '2011', 'message': 'Your booking is completed but not sent to the Master yet. Please verify your account to send your booking', 'booking_id': insertToLearnResult.insertId }
        }
        else {
            return { 'code': '201', 'message' : 'Your booking is sent to the Master. You can follow your booking status in your Learn profile', 'booking_id': insertToLearnResult.insertId }
        }
    }
    else {
        return { 'code': '500', 
        'message': 'We experienced an error with your booking. Please contact us for help' }
    }
}



async function fetchLearning(geniusId) {
    let query = `select ls.booking_id, ls.session_id, l.book_mode, ls.date_time, ls.address, phone_number, ls.status, clazz_title, clazz_demo_images, clazz_duration, id category_id, name category_name, icon category_icon, master_id, geniuses.full_name master_name, profile_image master_profile_image 
    from ${Tables.dbLearn} l, ${Tables.dbLearnSchedules} ls, ${Tables.dbClazzes}, ${Tables.dbGeniuses}, ${Tables.dbCategory} 
    where l.clazz_id = clazzes.clazz_id and l.genius_id = ${geniusId} and master_id = geniuses.genius_id and clazz_category = id and l.booking_id = ls.booking_id and (ls.status = '${clazzConstant.status.upcoming}' or ls.status = '${clazzConstant.status.waiting_for_approval}' or ls.status = '${clazzConstant.status.waiting_for_verification}')`

    let learnings = await database.executeQuery(query)
    let upcoming = []
    let completed = []

    learnings.forEach(function(learn) {        
        learn.clazz_demo_images = JSON.parse(learn.clazz_demo_images)
        learn.schedules = [learn.date_time]
        learn.clazz_address = learn.address
        delete learn.address

        let master = {}
        master.full_name = learn.master_name
        delete learn.master_name
        master.profile_image = learn.master_profile_image
        delete learn.master_profile_image

        learn.master = master
        
        if(learn.status == clazzConstant.status.completed) {
            completed.push(learn)
        }
        else {
            upcoming.push(learn)
        }
    }, this);

    learnings = {}
    learnings.upcoming_clazzes = upcoming
    learnings.completed_clazzes = completed

    return learnings


}


async function fetchSchedules(geniusId) {
    let query = `select ls.session_id, address, location_lat, location_long, date_time, clazz_title, full_name master_name, profile_image master_profile_image, c.clazz_duration, g.genius_id master_id, icon category_icon from ${Tables.dbLearn} l, ${Tables.dbGeniuses} g, ${Tables.dbClazzes} c, ${Tables.dbCategory} ca, ${Tables.dbLearnSchedules} ls
    where l.genius_id = ${geniusId} and l.clazz_id = c.clazz_id and c.master_id = g.genius_id and ls.booking_id = l.booking_id and c.clazz_category = ca.id and (ls.status = '${clazzConstant.status.upcoming}' or ls.status = '${clazzConstant.status.waiting_for_verification}' or ls.status = '${clazzConstant.status.waiting_for_approval}')`
    let schedules = await database.executeQuery(query)  

    if (schedules == null) { return null }

    schedules.forEach(function(element) {
        element.schedules = element.date_time
    }, this);
    
    return schedules
}




async function registerDeviceToGenius(deviceToken, geniusId) {

    // let tokens = await database.executeQuery(query)
    // if (tokens != null) {
    //     tokens = tokens.count > 0 ? tokens[0] : tokens
    // }

    // tokens += ';' + deviceToken

    let query = 'select device_token from ' + Tables.dbGeniuses + ' where genius_id = ' + geniusId
    query = 'UPDATE ' + Tables.dbGeniuses
    query += ' SET device_token = \"' + deviceToken + '\"'
    query += ' WHERE genius_id=' + geniusId
    
    let registered = await database.executeQuery(query)
    return registered

}


async function updateGeniusDetail(geniusId, data) {

    let updateString = Formatter.generateUpdateString(data)

    let query = 'UPDATE ' + Tables.dbGeniuses + ' SET '
    query += updateString
    query += ' WHERE genius_id=' + geniusId

    let result = await database.executeQuery(query)
    return result
}



async function fetchGeniusWithFacebookId(login_service_id) {
    let fetch = 'SELECT * FROM ' + Tables.dbGeniuses + ' WHERE login_service_id = ' + login_service_id

    try {
        let genius = await database.executeQuery(fetch)
        return (genius.length > 0) ? genius[0] : null
    } catch (error) {
        console.log(error)
        return null
    }
}

async function checkVerificationSubmitted(genius_id) {
    let query = `select exist(select 1 from ${Tables.dbGeniusVerification} where genius_id = ${genius_id}) does_exist`
    let result = await database.executeQuery(query)
    if (result.length > 0) { result = result[0] }
    else { return false }
    
    return result.does_exist
}

async function fetchSessions(booking_id) {
    let query = `select ls.date_time, ls.address, ls.booking_id, c.clazz_duration, c.clazz_title 
    from ${Tables.dbLearnSchedules} ls, ${Tables.dbLearn} l, ${Tables.dbClazzes} c 
    where ls.booking_id = ${booking_id} and ls.booking_id = l.booking_id and l.clazz_id = c.clazz_id 
    order by date_time asc`
}