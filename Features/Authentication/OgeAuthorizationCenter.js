const jwt                   = require('jsonwebtoken')
let geniusDatabase = require('../Genius/OgeGeniusDatabase')


module.exports.authorize = authorize
    

async function authorize(req) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization']

    if (token) {
        token = token.replace('Ogenii ', '')
        try {
            let data = await decodeJWT(token)
            let geniusId = data.genius_id
            let genius = await geniusDatabase.fetchGeniusDetail(geniusId)
            let isValidToken = token === genius.ogenii_token

            if (isValidToken) {
                let newToken = await renewTokenIfNeeded(data)
                if (newToken != null) { token = newToken }
            }

            return isValidToken ? genius : null
        } catch (err) {
            if (err.name == 'TokenExpiredError') {
                return null
            }
        }
        
        
    }
    else {
        return null
    }
}

async function renewTokenIfNeeded(data) {

    let now = new Date()
    now = now.getTime() / 1000
    let threeDays = 260000
    if (now < data.exp - threeDays) { return null } 
    
    let token = jwt.sign({ genius_id: data.genius_id, login_service_id: data.login_service_id }, config.secrect, {
        expiresIn: '60d'
    })

    let geninus = {
        genius_id: data.genius_id,
        ogenii_token: token        
    }

    geniusDatabase.updateGeniusTokenIntoDatabase(genius)
    return token
}

async function decodeJWT(token) {
    try {
        let decoded = await jwt.verify(token, config.secrect)
        return decoded ? decoded : null    
    } catch (error) {
        log(error)
        return null
    }
    
}
