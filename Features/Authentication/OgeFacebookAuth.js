let jwt                     = require('jsonwebtoken')
let geniusDatabase          = require('../Genius/OgeGeniusDatabase')
let common                  = require('../../Supporter/OgeCommon')

module.exports.loginWithFacebook = loginWithFacebook


async function loginWithFacebook(req, res) {

    let rawData = req.body
    if (rawData) {
        let genius = saveGeniusDetailFrom(rawData)
        let foundGenius = await geniusDatabase.fetchGeniusWithFacebookId(genius.login_service_id)
        if (foundGenius) {
            foundGenius = prepareReturnGeniusData(foundGenius)
            foundGenius = common.Formatter.removeUndefinedFieldsIn(foundGenius)
            res.send(common.Responser.response(200, foundGenius))
            renewTokenIfNeeded(foundGenius)
        }
        else {
            genius = await addNewGenius(genius)
            genius = prepareReturnGeniusData(genius)

            res.send(common.Responser.response(200, genius))

            geniusDatabase.updateGeniusTokenIntoDatabase(genius)
        }
    } else {
        res.send(common.Responser.response(500, 'Something wrong'))
    }
}

function saveGeniusDetailFrom(rawData) {
    var genius = {}
    genius.login_service_id      = rawData.login_service_id
    genius.login_service_token   = rawData.login_service_token
    genius.first_name       = rawData.first_name
    genius.last_name        = rawData.last_name
    let fullName            = genius.last_name + ' ' + genius.first_name
    genius.full_name        = fullName
    genius.birthday         = rawData.birthday
    genius.email            = rawData.email
    genius.profile_image    = rawData.profile_image
    genius.phone_number    = rawData.phone_number
    genius.device_token    = rawData.device_token
    genius.service_provider = rawData.service_provider

    return genius
}





function prepareReturnGeniusData(genius) {
    delete genius.login_service_id
    delete genius.login_service_token
    return genius
}

async function renewTokenIfNeeded(data) {
    
        let now = new Date()
        now = now.getTime() / 1000
        let threeDays = 260000
        if (now < data.exp - threeDays) { return } 
        
        let token = jwt.sign({ genius_id: data.genius_id, login_service_id: data.login_service_id }, config.secrect, {
            expiresIn: '60d'
        })
    
        let geninus = {
            genius_id: data.genius_id,
            ogenii_token: token        
        }
        geniusDatabase.updateGeniusTokenIntoDatabase(genius)
    }






//___________________________________________________ GENIUS DOESN'T EXIST ________________________________

async function addNewGenius(genius) {

    genius = await geniusDatabase.insertGeniusIntoDatabase(genius)
    let data = {
        'genius_id': genius.genius_id, 
        'login_service_id': genius.login_service_id
    }

    let token = generateTokenFrom(data)
    genius.ogenii_token = token

    return genius
}

function generateTokenFrom(data) {
    let token = jwt.sign({ genius_id: data.genius_id, login_service_id: data.login_service_id }, config.secrect, {
        expiresIn: '60d' // expires in 60 days
    })
    return token
}

