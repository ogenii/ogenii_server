let Tables                = require('../../DatabaseConnector/OgeDatabaseTables')
let clazzConstant         = require('../Clazzes/OgeClazzConstants')


module.exports.cancelClazz = cancelClazz
module.exports.getCancelPerson = getCancelPerson
module.exports.fetchDetailForNotificationPushing = fetchDetailForNotificationPushing


async function getCancelPerson(genius_id, session_id) {
    let query = `select l.booking_id, genius_id, master_id from ${Tables.dbLearn} l, ${Tables.dbClazzes} c, ${Tables.dbLearnSchedules} ls where session_id = ${session_id} and l.clazz_id = c.clazz_id and ls.booking_id = l.booking_id`
    let result = await database.executeQuery(query)
    if (result.length > 0) { result = result[0] }
    let data = result
    if (result.genius_id == genius_id) { 
        data.cancel_type = clazzConstant.status.genius_cancel 
        return data
    }
    if (result.master_id == genius_id) { 
        data.cancel_type = clazzConstant.status.master_cancel 
        return data
    }
    
    return 403
}

async function fetchDetailForNotificationPushing(data) {
    // data = genius_id, master_id, booking_id, status
    if (data.status == clazzConstant.status.master_cancel) {
        let query = `select full_name from ${Tables.dbGeniuses} where genius_id = ${data.master_id}`
        let master = await database.executeQuery(query)
        master = master[0]
        data.master_name = master.full_name 

        query = `select learn.clazz_id, clazz_title from ${Tables.dbClazzes} clazz, ${Tables.dbLearn} learn where booking_id = ${data.booking_id} and clazz.clazz_id = learn.clazz_id`
        let clazzDetail = await database.executeQuery(query)
        clazzDetail = clazzDetail[0]
        data.clazz_id = clazzDetail.clazz_id
        data.clazz_title = clazzDetail.clazz_title

        let content = `Master ${data.master_name} has just cancelled your class ${data.clazz_title}. Reason: ${data.reason} `
        data.content = content 
        return data
    }

    if (data.status == clazzConstant.status.genius_cancel) {
        let query = `select full_name from ${Tables.dbGeniuses} where genius_id = ${data.genius_id}`
        let genius = await database.executeQuery(query)
        genius = genius[0]
        data.genius_name = genius.full_name

        query = `select learn.clazz_id, clazz_title from ${Tables.dbClazzes} clazz, ${Tables.dbLearn} learn where booking_id = ${data.booking_id} and clazz.clazz_id = learn.clazz_id`
        let clazzDetail = await database.executeQuery(query)
        clazzDetail = clazzDetail[0]
        data.clazz_id = clazzDetail.clazz_id
        data.clazz_title = clazzDetail.clazz_title

        let content = `Genius ${data.genius_name} has just cancelled your class ${data.clazz_title}. Reason: ${data.reason} `
        data.content = content 
        return data
    }
}

async function cancelClazz(cancel_type, session_id, reason) {
    let today = new Date().yyyyMMddhhmm()
    let query = `update ${Tables.dbLearnSchedules} set status = '${cancel_type}', status_note = "${reason}", updated_at = '${today}' where session_id = ${session_id}`
    let result = await database.executeQuery(query)
    return result
}