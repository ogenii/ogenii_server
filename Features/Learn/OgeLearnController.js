let moment                = require('moment')
let AuthCenter            = require('../Authentication/OgeAuthorizationCenter')
let learnDatabase           = require('./OgeLearnDatabase')
let push                = require('../Notification/PushNotification')
let clazzConstant         = require('../Clazzes/OgeClazzConstants')

module.exports.cancelClazz = cancelClazz

async function cancelClazz(req, res) {
    let tracker = Tracker
    tracker.start()
    let genius = await AuthCenter.authorize(req)
    let isValid = genius != null 

    if (isValid) {
        tracker.log(genius.genius_id, genius.full_name, 'cancel')

        let data = req.body
        let cancelPerson = await learnDatabase.getCancelPerson(genius.genius_id, data.session_id)

        if (cancelPerson == 403) {
            let response = Responser.response(403, 'You do not booked this clazz')
            res.send(response)
            tracker.end(response, 'fail')
            return
        }
        
        let result = await learnDatabase.cancelClazz(cancelPerson.cancel_type, data.session_id, data.reason)
        
        let response = Responser.response(200, 'Cancelled')
        res.send(response)
        tracker.end(response, 'success')

        // push notification to master/genius 
        data.genius_id = cancelPerson.genius_id
        data.master_id = cancelPerson.master_id
        data.status = cancelPerson.cancel_type

        let pushData = await learnDatabase.fetchDetailForNotificationPushing(data)
        pushData = pushData[0]

        if (data.status == clazzConstant.status.genius_cancel) {
            push.pushGeniusCancelMessage(data)
            return 
        }

        if (data.status == clazzConstant.status.master_cancel) {
            push.pushMasterCancelMessage(data)
            return 
        }
    }
    else {
        tracker.log(null, 'Login needed', 'bookClazz')
        let response = Responser.response(401, { message: Responser.loginNeeded })
        res.send(response)
        tracker.end(response, 'fail')
    }
}