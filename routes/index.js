let express = require('express')

const router = express.Router()
const config = require('./../Supporter/OgeConfiguration')

// _________________________________ clazz_________________________________

var clazzController = require('./../Features/Clazzes/OgeClazzController')
router.get(config.baseUrl + '/clazz/:id', clazzController.fetchClazzDetail)
router.get(config.baseUrl + '/clazz/faq/:id', clazzController.fetchClazzFaq)
router.get(config.baseUrl + '/search/clazz/:query', clazzController.searchClazz)
router.get(config.baseUrl + '/search/clazz/', clazzController.searchClazz)
router.get(config.baseUrl + '/clazzes', clazzController.fetchClazzes)
router.post(config.baseUrl + '/clazz/create', clazzController.createClazz)
router.get(config.baseUrl + '/category/:id', clazzController.fetchClazzesInCategory)
router.get(config.baseUrl + '/clazz/receipt/:id', clazzController.fetchClazzReceipt)
router.post(config.baseUrl + '/clazz_room/add', clazzController.addClazzRoom)
router.get(config.baseUrl + '/subcategory/:id', clazzController.fetchSubcategory)
router.get(config.baseUrl + '/clazz_rooms', clazzController.fetchClazzRooms)
router.get(config.baseUrl + '/my_clazzes', clazzController.fetchLiveClazzes)

// _________________________________ MASTER _________________________________

var masterController = require('./../Features/Masters/OgeMasterController')
router.get(config.baseUrl + '/masters/available_time', masterController.fetchAvailableTime)
router.get(config.baseUrl + '/masters', masterController.fetchMasters)
// router.get(config.baseUrl + '/master/:id', masterController.fetchMasterDetail)
router.post(config.baseUrl + '/master/approve', masterController.approveRequestClazz)
router.post(config.baseUrl + '/master/decline', masterController.rejectRequestClazz)
router.post(config.baseUrl + '/master/register', masterController.becomeMaster)
router.post(config.baseUrl + '/master/add_expertise', masterController.addExpertises)
router.get(config.baseUrl + '/master/search/:query', masterController.searchMaster)
router.get(config.baseUrl + '/master/teaching_schedules', masterController.fetchTeachingSchedules)
router.get(config.baseUrl + '/master/subjects', masterController.getRegisteredTeachingSubject)


// _________________________________ AUTHENTICATION _________________________________

var facebookAuthentication = require('./../Features/Authentication/OgeFacebookAuth')
router.post(config.baseUrl + '/login', facebookAuthentication.loginWithFacebook)

// _________________________________ RESOURCES _________________________________
router.get('/cancellation', function (req, res) {
  res.sendFile(__dirname + './../Features/Resources/Cancellation.html')
})

// _________________________________ REVIEW _________________________________
var reviewController = require('./../Features/Review/OgeReviewController')
router.get(config.baseUrl + '/review/clazz', reviewController.fetchclazzReview)
router.post(config.baseUrl + '/review/clazz', reviewController.reviewclazz)

router.get(config.baseUrl + '/review/master', reviewController.fetchMasterReview)
router.post(config.baseUrl + '/review/master', reviewController.reviewMaster)

router.get(config.baseUrl + '/review/genius', reviewController.fetchGeniusReview)
router.post(config.baseUrl + '/review/genius', reviewController.reviewGenius)

// _________________________________ GENIUSES _________________________________
var geniusController = require('./../Features/Genius/OgeGeniusController')
router.post(config.baseUrl + '/book', geniusController.bookClazz)
router.get(config.baseUrl + '/genius/:id', geniusController.fetchGeniusDetail)
router.get(config.baseUrl + '/learn', geniusController.fetchLearningProgress)
router.put(config.baseUrl + '/genius/:id', geniusController.updateGeniusDetail)
router.post(config.baseUrl + '/genius/register_device', geniusController.registerDevice)
router.post(config.baseUrl + '/genius/verify', geniusController.verifyGenius)
router.get(config.baseUrl + '/genius_schedules', geniusController.fetchSchedules)


// _________________________________ NOTIFICATION _________________________________
var notificationController = require('./../Features/Notification/OgeNotificationController')
router.get(config.baseUrl + '/notifications', notificationController.fetchGeniusNotifcations)
router.get(config.baseUrl + '/nofitication/:page', notificationController.fetchGeniusNotifcations)
router.get(config.baseUrl + '/nofitication/mark-read/:notiId', notificationController.markSpecificNotiRead)
router.get(config.baseUrl + '/nofitication/mark-all-read', notificationController.markAllNotiRead)

let notification = require('./../Features/Notification/PushNotification')
// router.post(config.baseUrl + '/push_approved', notification.testPushApprovedMessage)
// router.post(config.baseUrl + '/push_rejected', notification.testPushRejectedMessageTo)
// router.post(config.baseUrl + '/push_booked', notification.testPushBookedMessageTo)



// ______________________________ DISCOVER __________________________________________
router.get(config.baseUrl + '/discover/top_view_clazzes', clazzController.discoverClazzes)


// ______________________________ LEARN __________________________________________
var learn = require('./../Features/Learn/OgeLearnController')
router.post(config.baseUrl + '/learn/cancel', learn.cancelClazz)


// ______________________________ ADMIN __________________________________________
var admin = require('./../Features/Admin/OgeAdminController')
router.get(config.baseUrl + '/admin/clazzes', admin.fetchPendingClazzes)
router.post(config.baseUrl + '/admin/approve_clazz', admin.approveClazz)
router.post(config.baseUrl + '/admin/reject_clazz', admin.rejectClazz)

router.get(config.baseUrl + '/admin/verifications', admin.fetchVerificationsPending)
router.get(config.baseUrl + '/admin/verification/:genius_id', admin.fetchSpecificVerification)
router.post(config.baseUrl + '/admin/approve_verification', admin.approveVerification)
router.post(config.baseUrl + '/admin/reject_verification', admin.rejectVerification)

router.get(config.baseUrl + '/admin/pending_masters', admin.fetchPendingMaster)
router.get(config.baseUrl + '/admin/pending_master/:master_id', admin.fetchSpecificPendingMaster)
router.post(config.baseUrl + '/admin/approve_master', admin.approveMaster)
router.post(config.baseUrl + '/admin/reject_master', admin.rejectMaster)

router.post(config.baseUrl + '/admin/test_push', admin.testPush)



// ______________________________ REPORT __________________________________________
var reporter = require('./../Features/Report/OgeErrorReportController')
router.post(config.baseUrl + '/silent_report', reporter.reportErrorSilenly)
router.post(config.baseUrl + '/need_help', reporter.needHelp)


module.exports = router
